package com.langejansen.trainer;

import com.google.gson.*;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by tim on 19-4-17.
 */
public abstract class TrainingData {
    Logger logger = Logger.getLogger("com.langejansen");

    protected abstract JsonObject toJson();
    public abstract boolean fromJson(JsonObject suppliedJson);

    @Override
    public String toString() {
        return toJson().toString();
    }

    /**
     * Simple training data: Data element with static inputs. No delta over time.
     */
    public static class simple extends TrainingData {
        private List<Double> inputData = new ArrayList<>();
        private HashMap<Integer, Double> outputData = new HashMap<>();
        private Boolean hasOutput = false;

        public simple() {
        }

        public simple(JsonObject suppliedJson) {
            fromJson(suppliedJson);
        }

        public simple(String jsonString) {
            fromString(jsonString);
        }

        public void fromString(String jsonString) {
            Gson gson = new Gson();
            fromJson(gson.fromJson(jsonString,JsonObject.class));
        }


        public Boolean hasOutPut() {
            return !outputData.isEmpty();
        }

        public Boolean hasInPut() {
            return !inputData.isEmpty();
        }

        public Double[] inputArray() {
            return inputData.toArray(new Double[inputData.size()]);
        }

        public HashMap<Integer, Double> outputs() {
            return outputData;
        }

        public void setInputs(Double[] supplied) {
            inputData.clear();
            inputData.addAll(Arrays.asList(supplied));
        }

        public void setOutputs(Double[] supplied) {
            outputData.clear();
            for (int i = 0; i < supplied.length; i++) {
                outputData.put(i, supplied[i]);
            }
        }

        public List<Double> inputs() {
            return inputData;
        }


        @Override
        public JsonObject toJson() {
            Gson g = new Gson();
            JsonObject retJ = new JsonObject();
            retJ.add("inputs", g.toJsonTree(inputArray()));
            retJ.add("outputs", g.toJsonTree(outputs()));
            return retJ;
        }

        public boolean equals(simple otherElement) {

            if (!Arrays.equals(inputArray(), otherElement.inputArray())) {
                return false;
            }

            HashMap<Integer, Double> otherOutputs = otherElement.outputs();
            for (Integer outputIndex : outputData.keySet()) {
                if (!outputData.get(outputIndex).equals(otherOutputs.getOrDefault(outputIndex, Double.NaN))) {
                    return false;
                }
            }
            for (Integer outputIndex : otherOutputs.keySet()) {
                if (!otherOutputs.get(outputIndex).equals(outputData.getOrDefault(outputIndex, Double.NaN))) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public boolean fromJson(JsonObject suppliedJson) {
            Gson g = new Gson();
            try {
                Double[] _inputData = g.fromJson(suppliedJson.getAsJsonArray("inputs"), Double[].class);
                HashMap<String, Double> _outputData = g.fromJson(suppliedJson.getAsJsonObject("outputs"), outputData.getClass());
                //todo: for some reason gson parses this collection to a <String, Double> instead of the outputted <Integer, Double>
                // Too lazy to make it output the correct type:
                HashMap<Integer, Double> properlyFormatted = new HashMap<>();
                for (Map.Entry<String, Double> entry : _outputData.entrySet()) {
                    properlyFormatted.put(Integer.parseInt(entry.getKey()), entry.getValue());
                }
                setInputs(_inputData);
                outputData.clear();
                outputData = properlyFormatted;
                return true;
            } catch (Exception e) {
                logger.warning("Malformed Json: " + e.getMessage());
                return false;
            }
        }
    }

    /**
     * Training data with delta t: Data element with inputs that vary over time
     */
    public static class deltaT extends TrainingData {
        private List<simple> _frames = new ArrayList<>();
        private Double msInterval;

        public deltaT(Double msInterval) {
            this.msInterval = msInterval;
        }

        public deltaT() {
        }

        public deltaT(JsonObject suppliedJson) {
            fromJson(suppliedJson);
        }

        public deltaT(String jsonString) {
            fromString(jsonString);
        }

        public boolean equals(deltaT obj) {
            if (getMsInterval() != obj.getMsInterval()) {
                return false;
            }
            List<simple> thisFrames = frames();
            List<simple> otherFrames = obj.frames();
            for (int n = 0;n<thisFrames.size();n++) {
                if (!thisFrames.get(n).equals(otherFrames.get(n))) {
                    return false;
                }
            }
            return true;
        }

        public void fromString(String jsonString) {
            Gson gson = new Gson();
            fromJson(gson.fromJson(jsonString,JsonObject.class));
        }

        public void save(String fileName) {
            JsonObject thisJson = toJson();
            FileWriter writer = null;
            try {
                writer = new FileWriter(fileName);
                //use gson for prettyPrinting.
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                writer.write(gson.toJson(thisJson));
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void load(String fileName) {
            Gson gson = new Gson();
            try {
                FileReader fr = new FileReader(fileName);
                Reader br = new BufferedReader(fr);
                //convert the json string back to object
                fromJson(gson.fromJson(br,JsonObject.class));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }

        public List<simple> frames() {
            return _frames;
        }

        /**
         * Returns the number of elements in this list.  If this list contains
         * more than <tt>Integer.MAX_VALUE</tt> elements, returns
         * <tt>Integer.MAX_VALUE</tt>.
         *
         * @return the number of elements in this list
         */

        public int size() {
            return _frames.size();
        }

        /**
         * Returns <tt>true</tt> if this list contains no elements.
         *
         * @return <tt>true</tt> if this list contains no elements
         */

        public boolean isEmpty() {
            return _frames.isEmpty();
        }

        /**
         * Returns <tt>true</tt> if this list contains the specified element.
         * More formally, returns <tt>true</tt> if and only if this list contains
         * at least one element <tt>e</tt> such that
         * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
         *
         * @param o element whose presence in this list is to be tested
         * @return <tt>true</tt> if this list contains the specified element
         * @throws ClassCastException   if the type of the specified element
         *                              is incompatible with this list
         *                              (<a href="Collection.html#optional-restrictions">optional</a>)
         * @throws NullPointerException if the specified element is null and this
         *                              list does not permit null elements
         *                              (<a href="Collection.html#optional-restrictions">optional</a>)
         */

        public boolean contains(simple o) {
            return _frames.contains(o);
        }

        /**
         * Returns an iterator over the elements in this list in proper sequence.
         *
         * @return an iterator over the elements in this list in proper sequence
         */

        public Iterator iterator() {
            return _frames.iterator();
        }

        /**
         * Returns an array containing all of the elements in this list in proper
         * sequence (from first to last element).
         * <p>
         * <p>The returned array will be "safe" in that no references to it are
         * maintained by this list.  (In other words, this method must
         * allocate a new array even if this list is backed by an array).
         * The caller is thus free to modify the returned array.
         * <p>
         * <p>This method acts as bridge between array-based and collection-based
         * APIs.
         *
         * @return an array containing all of the elements in this list in proper
         * sequence
         * @see Arrays#asList(Object[])
         */

        public simple[] toArray() {
            return _frames.toArray(new simple[size()]);
        }

        /**
         * Appends the specified element to the end of this list (optional
         * operation).
         * <p>
         * <p>Lists that support this operation may place limitations on what
         * elements may be added to this list.  In particular, some
         * lists will refuse to add null elements, and others will impose
         * restrictions on the type of elements that may be added.  List
         * classes should clearly specify in their documentation any restrictions
         * on what elements may be added.
         *
         * @param o element to be appended to this list
         * @return <tt>true</tt> (as specified by {@link Collection#add})
         * @throws UnsupportedOperationException if the <tt>add</tt> operation
         *                                       is not supported by this list
         * @throws ClassCastException            if the class of the specified element
         *                                       prevents it from being added to this list
         * @throws NullPointerException          if the specified element is null and this
         *                                       list does not permit null elements
         * @throws IllegalArgumentException      if some property of this element
         *                                       prevents it from being added to this list
         */

        public boolean add(simple o) {
            return _frames.add(o);
        }

        /**
         * Removes the first occurrence of the specified element from this list,
         * if it is present (optional operation).  If this list does not contain
         * the element, it is unchanged.  More formally, removes the element with
         * the lowest index <tt>i</tt> such that
         * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>
         * (if such an element exists).  Returns <tt>true</tt> if this list
         * contained the specified element (or equivalently, if this list changed
         * as a result of the call).
         *
         * @param o element to be removed from this list, if present
         * @return <tt>true</tt> if this list contained the specified element
         * @throws ClassCastException            if the type of the specified element
         *                                       is incompatible with this list
         *                                       (<a href="Collection.html#optional-restrictions">optional</a>)
         * @throws NullPointerException          if the specified element is null and this
         *                                       list does not permit null elements
         *                                       (<a href="Collection.html#optional-restrictions">optional</a>)
         * @throws UnsupportedOperationException if the <tt>remove</tt> operation
         *                                       is not supported by this list
         */

        public boolean remove(simple o) {
            return _frames.remove(o);
        }

        /**
         * Appends all of the elements in the specified collection to the end of
         * this list, in the order that they are returned by the specified
         * collection's iterator (optional operation).  The behavior of this
         * operation is undefined if the specified collection is modified while
         * the operation is in progress.  (Note that this will occur if the
         * specified collection is this list, and it's nonempty.)
         *
         * @param c collection containing elements to be added to this list
         * @return <tt>true</tt> if this list changed as a result of the call
         * @throws UnsupportedOperationException if the <tt>addAll</tt> operation
         *                                       is not supported by this list
         * @throws ClassCastException            if the class of an element of the specified
         *                                       collection prevents it from being added to this list
         * @throws NullPointerException          if the specified collection contains one
         *                                       or more null elements and this list does not permit null
         *                                       elements, or if the specified collection is null
         * @throws IllegalArgumentException      if some property of an element of the
         *                                       specified collection prevents it from being added to this list
         * @see #add(int, simple)
         */

        public boolean addAll(Collection c) {
            return _frames.addAll(c);
        }

        /**
         * Inserts all of the elements in the specified collection into this
         * list at the specified position (optional operation).  Shifts the
         * element currently at that position (if any) and any subsequent
         * elements to the right (increases their indices).  The new elements
         * will appear in this list in the order that they are returned by the
         * specified collection's iterator.  The behavior of this operation is
         * undefined if the specified collection is modified while the
         * operation is in progress.  (Note that this will occur if the specified
         * collection is this list, and it's nonempty.)
         *
         * @param index index at which to insert the first element from the
         *              specified collection
         * @param c     collection containing elements to be added to this list
         * @return <tt>true</tt> if this list changed as a result of the call
         * @throws UnsupportedOperationException if the <tt>addAll</tt> operation
         *                                       is not supported by this list
         * @throws ClassCastException            if the class of an element of the specified
         *                                       collection prevents it from being added to this list
         * @throws NullPointerException          if the specified collection contains one
         *                                       or more null elements and this list does not permit null
         *                                       elements, or if the specified collection is null
         * @throws IllegalArgumentException      if some property of an element of the
         *                                       specified collection prevents it from being added to this list
         * @throws IndexOutOfBoundsException     if the index is out of range
         *                                       (<tt>index &lt; 0 || index &gt; size()</tt>)
         */

        public boolean addAll(int index, Collection c) {
            return _frames.addAll(index, c);
        }

        /**
         * Removes all of the elements from this list (optional operation).
         * The list will be empty after this call returns.
         *
         * @throws UnsupportedOperationException if the <tt>clear</tt> operation
         *                                       is not supported by this list
         */

        public void clear() {
            _frames.clear();
        }

        /**
         * Returns the element at the specified position in this list.
         *
         * @param index index of the element to return
         * @return the element at the specified position in this list
         * @throws IndexOutOfBoundsException if the index is out of range
         *                                   (<tt>index &lt; 0 || index &gt;= size()</tt>)
         */

        public simple get(int index) {
            return _frames.get(index);
        }

        /**
         * Replaces the element at the specified position in this list with the
         * specified element (optional operation).
         *
         * @param index   index of the element to replace
         * @param element element to be stored at the specified position
         * @return the element previously at the specified position
         * @throws UnsupportedOperationException if the <tt>set</tt> operation
         *                                       is not supported by this list
         * @throws ClassCastException            if the class of the specified element
         *                                       prevents it from being added to this list
         * @throws NullPointerException          if the specified element is null and
         *                                       this list does not permit null elements
         * @throws IllegalArgumentException      if some property of the specified
         *                                       element prevents it from being added to this list
         * @throws IndexOutOfBoundsException     if the index is out of range
         *                                       (<tt>index &lt; 0 || index &gt;= size()</tt>)
         */

        public simple set(int index, simple element) {
            return _frames.set(index, element);
        }

        /**
         * Inserts the specified element at the specified position in this list
         * (optional operation).  Shifts the element currently at that position
         * (if any) and any subsequent elements to the right (adds one to their
         * indices).
         *
         * @param index   index at which the specified element is to be inserted
         * @param element element to be inserted
         * @throws UnsupportedOperationException if the <tt>add</tt> operation
         *                                       is not supported by this list
         * @throws ClassCastException            if the class of the specified element
         *                                       prevents it from being added to this list
         * @throws NullPointerException          if the specified element is null and
         *                                       this list does not permit null elements
         * @throws IllegalArgumentException      if some property of the specified
         *                                       element prevents it from being added to this list
         * @throws IndexOutOfBoundsException     if the index is out of range
         *                                       (<tt>index &lt; 0 || index &gt; size()</tt>)
         */

        public void add(int index, simple element) {
            _frames.add(index, element);
        }

        /**
         * Removes the element at the specified position in this list (optional
         * operation).  Shifts any subsequent elements to the left (subtracts one
         * from their indices).  Returns the element that was removed from the
         * list.
         *
         * @param index the index of the element to be removed
         * @return the element previously at the specified position
         * @throws UnsupportedOperationException if the <tt>remove</tt> operation
         *                                       is not supported by this list
         * @throws IndexOutOfBoundsException     if the index is out of range
         *                                       (<tt>index &lt; 0 || index &gt;= size()</tt>)
         */

        public simple remove(int index) {
            return null;
        }

        /**
         * Returns the index of the first occurrence of the specified element
         * in this list, or -1 if this list does not contain the element.
         * More formally, returns the lowest index <tt>i</tt> such that
         * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
         * or -1 if there is no such index.
         *
         * @param o element to search for
         * @return the index of the first occurrence of the specified element in
         * this list, or -1 if this list does not contain the element
         * @throws ClassCastException   if the type of the specified element
         *                              is incompatible with this list
         *                              (<a href="Collection.html#optional-restrictions">optional</a>)
         * @throws NullPointerException if the specified element is null and this
         *                              list does not permit null elements
         *                              (<a href="Collection.html#optional-restrictions">optional</a>)
         */

        public int indexOf(simple o) {
            return _frames.indexOf(o);
        }

        /**
         * Returns a list iterator over the elements in this list (in proper
         * sequence).
         *
         * @return a list iterator over the elements in this list (in proper
         * sequence)
         */

        public ListIterator listIterator() {
            return _frames.listIterator();
        }

        /**
         * Returns a list iterator over the elements in this list (in proper
         * sequence), starting at the specified position in the list.
         * The specified index indicates the first element that would be
         * returned by an initial call to {@link ListIterator#next next}.
         * An initial call to {@link ListIterator#previous previous} would
         * return the element with the specified index minus one.
         *
         * @param index index of the first element to be returned from the
         *              list iterator (by a call to {@link ListIterator#next next})
         * @return a list iterator over the elements in this list (in proper
         * sequence), starting at the specified position in the list
         * @throws IndexOutOfBoundsException if the index is out of range
         *                                   ({@code index < 0 || index > size()})
         */

        public ListIterator listIterator(int index) {
            return _frames.listIterator(index);
        }

        /**
         * Returns a view of the portion of this list between the specified
         * <tt>fromIndex</tt>, inclusive, and <tt>toIndex</tt>, exclusive.  (If
         * <tt>fromIndex</tt> and <tt>toIndex</tt> are equal, the returned list is
         * empty.)  The returned list is backed by this list, so non-structural
         * changes in the returned list are reflected in this list, and vice-versa.
         * The returned list supports all of the optional list operations supported
         * by this list.<p>
         * <p>
         * This method eliminates the need for explicit range operations (of
         * the sort that commonly exist for arrays).  Any operation that expects
         * a list can be used as a range operation by passing a subList view
         * instead of a whole list.  For example, the following idiom
         * removes a range of elements from a list:
         * <pre>{@code
         *      list.subList(from, to).clear();
         * }</pre>
         * Similar idioms may be constructed for <tt>indexOf</tt> and
         * <tt>lastIndexOf</tt>, and all of the algorithms in the
         * <tt>Collections</tt> class can be applied to a subList.<p>
         * <p>
         * The semantics of the list returned by this method become undefined if
         * the backing list (i.e., this list) is <i>structurally modified</i> in
         * any way other than via the returned list.  (Structural modifications are
         * those that change the size of this list, or otherwise perturb it in such
         * a fashion that iterations in progress may yield incorrect results.)
         *
         * @param fromIndex low endpoint (inclusive) of the subList
         * @param toIndex   high endpoint (exclusive) of the subList
         * @return a view of the specified range within this list
         * @throws IndexOutOfBoundsException for an illegal endpoint index value
         *                                   (<tt>fromIndex &lt; 0 || toIndex &gt; size ||
         *                                   fromIndex &gt; toIndex</tt>)
         */

        public List subList(int fromIndex, int toIndex) {
            return _frames.subList(fromIndex, toIndex);
        }

        /**
         * Removes from this list all of its elements that are contained in the
         * specified collection (optional operation).
         *
         * @param c collection containing elements to be removed from this list
         * @return <tt>true</tt> if this list changed as a result of the call
         * @throws UnsupportedOperationException if the <tt>removeAll</tt> operation
         *                                       is not supported by this list
         * @throws ClassCastException            if the class of an element of this list
         *                                       is incompatible with the specified collection
         *                                       (<a href="Collection.html#optional-restrictions">optional</a>)
         * @throws NullPointerException          if this list contains a null element and the
         *                                       specified collection does not permit null elements
         *                                       (<a href="Collection.html#optional-restrictions">optional</a>),
         *                                       or if the specified collection is null
         * @see #remove(int)
         * @see #contains(simple)
         */

        public boolean removeAll(Collection c) {
            return _frames.removeAll(c);
        }

        public Double getMsInterval() {
            return msInterval;
        }

        public void setMsInterval(Double msInterval) {
            this.msInterval = msInterval;
        }

        @Override
        public JsonObject toJson() {
            JsonObject retObj = new JsonObject();
            JsonArray dataCollection = new JsonArray();
            for (TrainingData dataFrame: _frames) {
                JsonObject frameJson = dataFrame.toJson();
                frameJson.addProperty("index", _frames.indexOf(dataFrame));
                dataCollection.add(dataFrame.toJson());
            }
            retObj.add("frames",dataCollection);
            retObj.addProperty("msInterval", msInterval);
            return retObj;
        }


        @Override
        public boolean fromJson(JsonObject suppliedJson) {
            try {
                Gson g = new Gson();
                for (JsonElement frame : suppliedJson.getAsJsonArray("frames")) {
                    JsonObject frameObj = frame.getAsJsonObject();
                    if (frameObj.has("index")) {
                        int index = frameObj.get("index").getAsInt();
                        add(index,new simple(frame.getAsJsonObject()));
                    } else {
                        add(new simple(frame.getAsJsonObject()));

                    }
                }
                if (!suppliedJson.get("msInterval").isJsonNull()) {
                    msInterval = suppliedJson.get("msInterval").getAsDouble();
                }
                return true;
            } catch (Exception e) {
                logger.warning("Jason not valid: " + e.getMessage());
                return false;
            }
        }


    }
}
