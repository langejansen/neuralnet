package com.langejansen.trainer;

import com.langejansen.net.CarrierCollection;
import com.langejansen.net.CellCollection;
import com.langejansen.net.StressorCollection;
import com.langejansen.net.cellClass;
import com.langejansen.net.support.AllCells;

import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Trains a neural net to the supplied data.
 */
public abstract class Trainer {
    private final TrainingData.deltaT trainingData;
    public CellCollection targetNet;
    public StressorCollection stressors;

    private Double _threshold = 0.05d;
    private cellClass inputPoint;
    private Logger logger = Logger.getLogger(getClass().getName());

    /**
     * @param targetNet the neural net this trainer will work on
     * @param trainingData the data used in training
     */
    public Trainer(CellCollection targetNet, TrainingData.deltaT trainingData) {
        this.targetNet = targetNet;
        this.trainingData = trainingData;
    }

    private static HashMap<Integer, Double> deviationMap(HashMap<Integer, Double> outputs, HashMap<Integer, Double> expected) {
        HashMap<Integer, Double> retMap = new HashMap<>();

        for (Integer outputIndex : outputs.keySet()) {
            Double deviation = 0d;
            if (!outputs.get(outputIndex).equals(expected.get(outputIndex))) {
                deviation = (expected.get(outputIndex) - outputs.get(outputIndex)) / expected.get(outputIndex);
            }
            retMap.put(outputIndex, deviation);
        }
        return retMap;
    }

    private static Double[] deviationMapArray(HashMap<Integer, Double> outputs, HashMap<Integer, Double> expected) {
        return map2DoubleArray(deviationMap(outputs, expected));
    }

    private static Double[] map2DoubleArray(HashMap<Integer, Double> inputMap) {
        int maxIndex = max(inputMap.keySet().toArray(new Integer[inputMap.size()]));
        Double[] retValues = new Double[maxIndex + 1];
        for (int retValueIndex : inputMap.keySet()) {
            retValues[retValueIndex] = inputMap.get(retValueIndex);
        }
        return retValues;
    }

    private static Integer max(Integer[] values) {
        Integer retVal = Integer.MIN_VALUE;
        for (Integer value : values) {
            retVal = Math.max(retVal, value);
        }
        return retVal;
    }

    public TrainingData.deltaT getTrainingData() {
        return trainingData;
    }

    public abstract Double train();

    public Double getThreshold() {
        return _threshold;
    }

//    private void initInputPoint() {
//        initInputPoint(false);
//    }
//
//    private void initInputPoint(boolean force) {
//        if (inputPoint == null || force) {
//            inputPoint = targetNet.addCell(new cellClass.basic("inputPoint"));
//            if (force) {
//                inputPoint.disconnect();
//            }
//            HashMap<Integer, String> inputs = targetNet.getInputs();
//            for (Integer inputCellIndex : inputs.keySet()) {
//                String inputCellName = inputs.get(inputCellIndex);
//                logger.info("Unknown input " + inputCellName + " with index " + inputCellIndex + "added");
//                if (targetNet.getCell(inputCellName) == null) {
//                    cellClass newInput = targetNet.addCell(new cellClass.basic(inputCellName));
//                    newInput.setInput(inputCellIndex);
//                }
//                targetNet.carriers().connect("inputPoint", inputCellName);
//            }
//        }
//    }
//
//    //todo: test applinput/getoutputarray
//    private void applyInput(Double[] inputs) {
//        //make the input emit from a basic point/cell so the pulses start at the same time (as much as possible)
//        initInputPoint();
//        HashMap<Integer, String> inputNames = targetNet.getInputs();
//        for (Integer inputIndex : inputNames.keySet()) {
//            try {
//                targetNet.carriers().setBias("inputPoint", inputNames.get(inputIndex), inputs[inputIndex]);
//            } catch (errors.notConnectedError notConnectedError) {
//                logger.severe("unknown input found. refreshing inputs");
//                initInputPoint(true);
//                //do this again
//                applyInput(inputs);
//                return;
//            }
//        }
//        inputPoint.sendPulse(1d);
//    }

    public void setThreshold(Double _threshold) {
        this._threshold = _threshold;
    }

    public static class backpropagating extends Trainer {
        private Logger logger = Logger.getLogger(getClass().getName());

        private Double stressLevel = 0.1d;

        /**
         * @param targetNet    the neural net this trainer will work on
         * @param trainingData the data used in training
         */
        public backpropagating(CellCollection targetNet, TrainingData.deltaT trainingData) {
            super(targetNet, trainingData);
        }

        @Override
        public Double train() {
            AllCells allCells = AllCells.getInstance();
            CarrierCollection allConnections = allCells.carriers();

            Double generalDeviation = Double.POSITIVE_INFINITY;
            //lay a backpropagating stressor net over the target net
            stressors = new StressorCollection.propagating(targetNet, true);
            //identify/mark inputs/outputs in CellCollection
            targetNet.refreshInputsOutputs();

            //While the general deviation is *not* below the threshold:
            Integer run = 0;
            while (generalDeviation > getThreshold()) {
                run++;
                //loop over every frame in the trainingdata:
                Integer frameNum = 0;
                for (TrainingData.simple frame : getTrainingData().frames()) {
                    frameNum++;
                    //??apply noise?
                    //supply input
                    {
                        targetNet.applyInput(frame.inputArray());
                        //leave to process or until no pulses are active for N ms
                        allConnections.pulseQueue.winddown(1000);
                    }
                    //evaluate the response
                    //calculate the deviation from the training data for each output
                    Double[] deviations = deviationMapArray(targetNet.getOutputValues(), frame.outputs());
                    logger.info(run + "." + frameNum + ":\t" + Arrays.toString(deviations));


                    //apply the training
                    //apply the deviation as a stress pulse to each output
                    targetNet.applyStress(deviations, getStressLevel());
                    //leave to process or until no pulses are active for N ms
                    allConnections.pulseQueue.winddown(1000);

                    //reset net
                    targetNet.relax();
                    stressors.relax();
                }

                //calculate the general deviation:
                //loop over every frame in the trainingdata(again)
                for (TrainingData.simple frame : getTrainingData().frames()) {
                    //supply input
                    //pulse the input value into the net
                    //leave to process or until no pulses are active for N ms
                    //evaluate the response
                    //calculate the deviation from the training data for each output
                    //reset net
                }
                //average the total deviation to a general deviation
            }

            //return the general deviation
            return generalDeviation;
        }

        public Double getStressLevel() {
            return stressLevel;
        }

        public void setStressLevel(Double stresslevel) {
            stressLevel = stresslevel;
        }

    }
}
