package com.langejansen;

import com.langejansen.net.cellClass;
import com.langejansen.net.support.AllCells;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

/**
 * Created by Tim Jansen @2017
 */
public class Main {

    /**
     * @param args commandline arguments
     */
    public static void main(String[] args) {
        Logger logger = Logger.getLogger("com.langejansen");
        Handler logHandler = new Handler() {
            @Override
            public void publish(LogRecord record) {
                System.out.print(getFormatter().format(record));
            }

            @Override
            public void flush() {
                System.out.flush();
                System.err.flush();
            }

            @Override
            public void close() throws SecurityException {
            }
        };
        Formatter logFormatter = new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getLevel() + "\t" + "[" + Thread.currentThread().getName() + "]" + record.getLoggerName()+"->"+record.getSourceMethodName() + " : \t" + record.getMessage()+ "\n";
            }
        };
        logHandler.setFormatter(logFormatter);
        logger.setUseParentHandlers(false);
        logger.addHandler(logHandler);
        logger.setLevel(Level.SEVERE);
        List<cellClass.basic> cells = new ArrayList<>();

        cells.add(new cellClass.basic("c1"));
        for (Integer n = 2;n<=10000;n++) {
            cellClass.basic newCell = new cellClass.basic("c" + n);
            cells.get(cells.size()-1).connectCell(newCell);
            cells.add(newCell);
        }
        cellClass.basic c1 = cells.get(0);
        cellClass.basic cLast = cells.get(cells.size()-1);

        for (Integer n = 1;n<1000;n++) {
            cells.get(((Double) (Math.random()*10000)).intValue()).connectCell(cells.get(((Double) (Math.random()*10000)).intValue()));
        }
        c1.sendPulse(1d);

        AllCells.getInstance().carriers().pulseQueue.winddown(100);

        for (Integer n = 1;n<10;n++) {
            cells.get(1).connectCell(cells.get(((Double) (Math.random()*10000)).intValue()));
        }
        AllCells.getInstance().save("beaintest.json");
    }
}
