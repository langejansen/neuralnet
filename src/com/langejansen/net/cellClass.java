package com.langejansen.net;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.langejansen.net.support.AllCells;
import com.langejansen.net.support.AllStressors;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

/**
 * Created by tim on 18-4-17.
 */
public abstract class cellClass {
    public stressor cellStressor;
    private String cellName;
    private Double potential = 0d;
    private Double triggerLevel = 1d;
    private CarrierCollection carriers = AllCells.getInstance().carriers();
    private HashMap<String, Double> connectionBias = new HashMap<>();
    private boolean deactivated = false;
    private Logger logger = Logger.getLogger(getClass().getName());
    private boolean backPropagating = false;
    private Integer input = -1;
    private Integer output = -1;
    private List<Pulse> incomingPulses = null;
    private List<Pulse> outgoinPulses = null;
    private boolean _incomingPulsesClaimed = false;

    /**
     * Cell with default settings, name defaults to the object hashID
     */
    public cellClass() {
        init();
    }

    private void init() {
        if (cellName == null) {
            cellName = UUID.randomUUID().toString();
        }
        AllCells allCells = AllCells.getInstance();
        allCells.addCell(this);
    }

    /**
     * @param cellName name of the cell
     */
    public cellClass(String cellName) {
        this.cellName = cellName;
        init();
    }

    /**
     * load the cell properties, including connections and bias, from a json object.
     *
     * @param jsonObject a json object containing the needed info
     */
    public cellClass(JsonObject jsonObject) {
        this(jsonObject, true);
    }

    /**
     * load the cell properties, including connections and bias, from a json object.
     *
     * @param jsonObject  a json object containing the needed info
     * @param connectBias should bias' loaded from the json be registered as connections too
     */
    public cellClass(JsonObject jsonObject, boolean connectBias) {
        fromJSON(jsonObject);
        init();
        if (connectBias) {
            //if there are bias' loaded from the json, then register the connections too
            biasToConnections();
        }
    }

    private void biasToConnections() {
        AllCells allCells = AllCells.getInstance();

        //if there are bias' loaded from the json, then register the connections too
        for (String connectedCell : connectionBias.keySet()) {
            connectCell(allCells.getCell(connectedCell));
        }
    }

    /**
     * connect this cellClass instance to a target instance
     *
     * @param targetCell target cellClass instance
     */
    public void connectCell(cellClass targetCell) {
        carriers.connect(this, targetCell);
    }

    /**
     * Load a JsonObject as these cell values
     *
     * @param j Object with the data
     */
    public void fromJSON(JsonObject j) {
        Gson gson = new Gson();
        Class t;
        //loaded json should be fior the same type
        assert j.get("type").getAsString().equals(getClass().getSimpleName());

        cellName = j.get("cellName").getAsString();
        potential = j.get("potential").getAsDouble();
        triggerLevel = j.get("triggerLevel").getAsDouble();
        JsonArray connectionBiasJson = j.get("connectionBias").getAsJsonArray();
        connectionBias.clear();
        for (JsonElement line : connectionBiasJson) {
            JsonObject lineObj = line.getAsJsonObject();
            connectionBias.put(lineObj.get("key").getAsString(), lineObj.get("value").getAsDouble());
        }
    }

    public List<Pulse> getIncomingPulses() {
        if (incomingPulses == null) {
            incomingPulses = new ArrayList<>();
        }
        return incomingPulses;
    }

    public void setIncomingPulses(List<Pulse> incomingPulses) {
        this.incomingPulses = incomingPulses;
    }

    public boolean incomingPulsesClaimed() {
        return _incomingPulsesClaimed;
    }

    public void setIncomingPulsesClaimed(boolean value) {
        _incomingPulsesClaimed = value;
    }

    public List<Pulse> getOutgoinPulses() {
        if (outgoinPulses == null) {
            outgoinPulses = new ArrayList<>();
        }
        return outgoinPulses;
    }

    public void setOutgoinPulses(List<Pulse> outgoinPulses) {
        this.outgoinPulses = outgoinPulses;
    }

    /**
     * make this unresponsive to any pulses
     */
    public void deactivate() {
        deactivated = true;
    }

    public void del() {
        AllCells.getInstance().removeCell(this);
    }

    public void setCellName(String cellName) {
        this.cellName = cellName;
    }

    /**
     * Set the connection bias
     *
     * @param cellNAme target cell
     * @param bias     bias value
     */
    public void setConnectionBias(String cellNAme, Double bias) {
        connectionBias.put(cellNAme, bias);
        logger.fine("bias: " + cellNAme + "->" + getName() + "\t= " + bias);
    }

    public String getName() {
        return cellName;
    }

    /**
     * Get the connection bias
     *
     * @param cellName target cell
     * @return bias value
     */
    public double getConnectionBias(String cellName) {
        return connectionBias.get(cellName);
    }

    @Override
    public String toString() {
        return toJSON().toString();
    }

    /**
     * @return JSON formatted string of this instance
     */
    public JsonObject toJSON() {
        connectionBiasPopulate();
        Gson gson = new Gson();
        JsonObject j = new JsonObject();
        j.addProperty("cellName", getName());
        j.addProperty("potential", getPotential());
        j.addProperty("triggerLevel", getTriggerLevel());
        j.add("connectionBias", HashMapToJson(getConnectionBias()));
        j.addProperty("type", getClass().getSimpleName());
        return j;
    }

    public Double getPotential() {
        return potential;
    }

    public void setPotential(Double potential) {
        this.potential = potential;
    }

    public Double getTriggerLevel() {
        return triggerLevel;
    }

    public void setTriggerLevel(Double triggerLevel) {
        potential = 0d;
        this.triggerLevel = triggerLevel;
        logger.finest(cellName + " triggerLevel set to " + triggerLevel);
    }

    public HashMap<String, Double> getConnectionBias() {
        return connectionBias;
    }

    /**
     * populate the connection bias' with the default weight of 1d for performance or exporting.
     */
    private void connectionBiasPopulate() {
        for (cellClass cell : carriers.connectedTo(this)) {
            getConnectionBias(cell.getName(), 1d);
        }
    }

    /**
     * Get the connection bias with a default. bias is also registered if unknown
     *
     * @param cellName    target cell
     * @param defaultBias default bias value
     * @return bias value
     */
    public double getConnectionBias(String cellName, Double defaultBias) {
        Double retDouble = connectionBias.getOrDefault(cellName, Double.NaN);
        if (retDouble.isNaN()) {
            connectionBias.put(cellName, defaultBias);
            logger.finest("unweighted connection created: " + getName() + " -> " + cellName + " = \t" + defaultBias);
            return defaultBias;
        }
        return retDouble;
    }

    private JsonArray HashMapToJson(HashMap sourceMap) {
        JsonArray retJ = new JsonArray();
        for (Object keyVal : sourceMap.keySet()) {
            JsonObject obj = new JsonObject();
            obj.addProperty("key", (String) keyVal);
            Object value = sourceMap.get(keyVal);
            if (value.getClass() == String.class) {
                obj.addProperty("value", (String) value);
            } else if (value.getClass() == Double.class) {
                obj.addProperty("value", (Double) value);
            } else {
                obj.addProperty("value", value.toString());
            }
            retJ.add(obj);
        }
        return retJ;
    }

    /**
     * connect to this cellClass instance from a target instance (reverse of connectCell)
     *
     * @param targetCell target cellClass instance
     */
    public void connectFromCell(cellClass targetCell) {
        carriers.connect(targetCell, this);
        logger.fine("Connected: " + targetCell.cellName + " -> " + cellName);
    }

    /**
     * Process a pulse. (add potential, emit when reached triggerlevel)
     *
     * @param pulse recieved pulse
     */
    public void receivePulse(Pulse pulse) {
        if (getDeactivated()) {
            return;
        }
        logger.finer("Collected pulse: " + pulse.emittingID + " -> " + getName());
        Double connectionBias = pulse.getBias(this, 1d);
        addPotential(pulse.intensity * connectionBias);

        if (cellStressor != null) {
            cellStressor.receivePulse(pulse);
        }
        if (getPotential() >= getTriggerLevel()) {
            sendPulse(getTriggerLevel());
            setPotential(0d);
        }
    }

    /**
     * is this unresponsive to any pulses
     */
    public boolean getDeactivated() {
        return deactivated;
    }

    /**
     * process received potential, re-emit a pulse it the triggerlevel has been reached
     *
     * @param addedPotential the recieved potential
     */
    public void addPotential(Double addedPotential) {
        potential += addedPotential;
    }

    /**
     * emit a pulse
     *
     * @param intensity intensity of the emitting pulse
     */
    public void sendPulse(Double intensity) {
        if (!backPropagating) {
            sendPulse(new Pulse.pullmodel(cellName, intensity));
        } else {
            sendPulse(new Pulse.pullmodel.backpropagating(cellName, intensity));
        }
    }

    /**
     * emit a pulse
     *
     * @param pulse the pulse to emit
     */
    public void sendPulse(Pulse pulse) {
        pulse.emittingID = cellName;
        carriers.send(pulse);
    }

    public Boolean awaitPulse(Integer timeSeconds) throws TimeoutException {
        return timePulseMs(timeSeconds) != null;
    }

    public Long timePulseMs(Integer timeOut) throws TimeoutException {
        logger.fine("wait for " + timeOut + " seconds on a pulse");
        long minRunTime = timeOut * 1000;
        long startTime = System.currentTimeMillis();
        long lastChecked = startTime;
        double startPotential = getPotential();
        while (true) {
            if (getPotential() != startPotential) {
                Long runTimeS = (System.currentTimeMillis() - startTime) / 1000;
                Double runTimeSDouble = ((Long) (System.currentTimeMillis() - startTime)).doubleValue()/1000d;
                String runTimeStr = runTimeSDouble.toString();
                logger.fine("pulse recieved after " + runTimeStr + " sec. Potential is now " + getPotential());
                return System.currentTimeMillis() - startTime;
            }
            if (System.currentTimeMillis() - startTime >= minRunTime) {
                logger.fine("no pulse recieved within " + timeOut);
                return null;
            }
            if (System.currentTimeMillis() - lastChecked >= 10000) {
                Long runTimeS = (System.currentTimeMillis() - startTime) / 1000;
                String runTimeStr = ((Integer) runTimeS.intValue()).toString();
                logger.info("\t " + runTimeStr + "\tsec");
                lastChecked = System.currentTimeMillis();
            }
//            Thread.yield();
        }

    }

    public void disconnect() {
        AllCells.getInstance().carriers().disconnect(this);
    }

    public boolean getBackPropagating() {
        return backPropagating;
    }

    public void setBackPropagating(boolean backPropagating) {
        this.backPropagating = backPropagating;
    }

    public Integer isInput() {
        return input;
    }

    public Integer isOutput() {
        return output;
    }

    public void setOutput(int output) {
        this.output = output;
        setTriggerLevel(Double.MAX_VALUE);
        logger.fine("set as output: " + getName());
    }

    public void setInput(int input) {
        this.input = input;
        logger.fine("set as input: " + getName());
    }

    /**
     * A 'basic' cell class that logs pulses it sends
     */
    public static class basicLogging extends basic {
        List<Pair<Long, Pulse>> recievedHistory = new ArrayList<>();
        private Logger logger = Logger.getLogger(getClass().getName());

        public basicLogging(String loggingcell) {
            super(loggingcell);
        }

        //todo: log the pulses

        private void logPulse(Pulse pulse) {
            //todo: log this pulse
        }

        @Override
        public void sendPulse(Pulse pulse) {
            super.sendPulse(pulse);
            logger.info("basicLoggingPulse");
        }


        // TODO: 21-6-17 diminish connections based on stress and time passed since pulses 
    }

    /**
     * A basic cell class that responds to a pulse by adding it to its potential. Emits a puls when potential has reached the triggerlevel.
     */
    public static class basic extends cellClass {
        private Logger logger = Logger.getLogger(getClass().getName());

        /**
         * Cell with default settings, name defaults to the object hashID
         */
        public basic() {
            super();
        }

        /**
         * @param cellName name of the cell
         */
        public basic(String cellName) {
            super(cellName);
        }

        /**
         * load the cell properties, including connections and bias, from a json object.
         *
         * @param jsonObject a json object containing the needed info
         */
        public basic(JsonObject jsonObject) {
            super(jsonObject);
        }

        /**
         * load the cell properties, including connections and bias, from a json object.
         *
         * @param jsonObject  a json object containing the needed info
         * @param connectBias should bias' loaded from the json be registered as connections too
         */
        public basic(JsonObject jsonObject, boolean connectBias) {
            super(jsonObject, connectBias);
        }

    }

    /**
     * A basic cell class that responds to a pulse by adding it to its potential.
     * Emits that potential when potential has reached the triggerlevel.
     */
    public static class basicAnalog extends cellClass {
        private Logger logger = Logger.getLogger(getClass().getName());

        /**
         * Cell with default settings, name defaults to the object hashID
         */
        public basicAnalog() {
            super();
        }

        /**
         * @param cellName name of the cell
         */
        public basicAnalog(String cellName) {
            super(cellName);
        }

        /**
         * load the cell properties, including connections and bias, from a json object.
         *
         * @param jsonObject a json object containing the needed info
         */
        public basicAnalog(JsonObject jsonObject) {
            super(jsonObject);
        }

        /**
         * load the cell properties, including connections and bias, from a json object.
         *
         * @param jsonObject  a json object containing the needed info
         * @param connectBias should bias' loaded from the json be registered as connections too
         */
        public basicAnalog(JsonObject jsonObject, boolean connectBias) {
            super(jsonObject, connectBias);
        }


        /**
         * Process a pulse. (add potential, emit when reached triggerlevel)
         *
         * @param pulse recieved pulse
         */
        @Override
        public void receivePulse(Pulse pulse) {
            if (getDeactivated()) {
                return;
            }
            logger.finer("Collected pulse: " + pulse.emittingID + " -> " + getName());
            Double connectionBias = pulse.getBias(this, 1d);
            addPotential(pulse.intensity * connectionBias);

            if (cellStressor != null) {
                cellStressor.receivePulse(pulse);
            }
            if (getPotential() >= getTriggerLevel()) {
                sendPulse(getPotential());
                setPotential(0d);
            }
        }
    }

    /**
     * A stressor influences a connection or a cell
     */
    public static class stressor {
        /**
         * if true: the stressor influences the cell bias/triggerlevel
         * if false: the stressor influences the connection bias'
         */
        public Boolean cellStressor = false;
        private Logger logger = Logger.getLogger(getClass().getName());
        private AllStressors allStressors = AllStressors.getInstance();
        private cellClass _associatedCell;
        private basicAnalog _stressCell;
        private Boolean adoptBias = true;
        private boolean backPropagating = true;

        /**
         * Cell with default settings, name defaults to the object hashID
         */
        public stressor(cellClass associatedCell) {
            _stressCell = new basicAnalog("$-" + associatedCell.getName());
            _stressCell.setBackPropagating(backPropagating);
            _associatedCell = associatedCell;
            _stressCell.cellStressor = this;
            init();
        }

        /**
         * run at end of constructor
         */
        private void init() {
            allStressors.add(this);
        }

        /**
         * load the cell properties, including connections and bias, from a json object.
         *
         * @param jsonObject a json object containing the needed info
         */
        public stressor(JsonObject jsonObject) {
            fromJson(jsonObject);
        }

        public void fromJson(JsonObject jsonObject) {
            _associatedCell = AllCells.getInstance().getCell(jsonObject.get("associatedCell").getAsString());
            _stressCell = (basicAnalog) AllCells.getInstance().getCell(jsonObject.get("stressCell").getAsString());
            init();
        }

        /**
         * load the cell properties, including connections and bias, from a json object.
         *
         * @param jsonObject  a json object containing the needed info
         * @param connectBias should bias' loaded from the json be registered as connections too
         */
        public stressor(JsonObject jsonObject, boolean connectBias) {
            fromJson(jsonObject);
        }

        public void collectAndAdoptBias() {
            if (!getAdoptBias()) {
                return;
            }

            //collect the connections we need to make
            AllCells allCells = AllCells.getInstance();
            CarrierCollection allCarriers = allCells.carriers();
            class connection {
                private String from;
                private String to;
                private Double bias;

                public connection(String from, String to) {
                    this.from = from;
                    this.to = to;
                    bias = allCells.getCell(this.from).getConnectionBias(this.to, 1d);
                }

                public void reverse() {
                    String origFrom = from;
                    String origTo = to;
                    to = origTo;
                    from = origFrom;
                }

                public void copyToStressors() {
                    stressor fromS = fromStressor();
                    cellClass fromSC = fromS.getCell();

                    stressor toS = toStressor();
                    cellClass toSC = toS.getCell();
                    allCarriers.connect(fromSC, toSC);
                }

                public stressor toStressor() {
                    return allStressors.getStressorFor(to);
                }

                public stressor fromStressor() {
                    return allStressors.getStressorFor(from);
                }

                public String getFrom() {
                    return from;
                }

                @Override
                public String toString() {
                    return "{from:'" + from + "', to:'" + to + "', bias:" + bias + "}";
                }

                public String getTo() {
                    return to;
                }

                public Double getBias() {
                    return bias;
                }


            }

            List<connection> connections = new ArrayList<>();

            cellClass assocCell = _associatedCell;
            String assocName = assocCell.getName();

            List<String> connectedToNames = allCarriers.connectedToNames(assocName);
            for (String cellName : connectedToNames) {
                connections.add(new connection(assocName, cellName));
            }
            List<String> connectedFromNames = allCarriers.connectedFromNames(assocName);
            for (String cellName : connectedFromNames) {
                connections.add(new connection(cellName, assocName));
            }

            //no connections? just leave.
            if (connections.size() == 0) {
                return;
            }

            for (connection conn : connections) {
                conn.copyToStressors();
            }

        }

        public JsonObject toJson() {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("associatedCell", _associatedCell.getName());
            jsonObject.addProperty("stressCell", _stressCell.getName());
            return jsonObject;
        }

        /**
         * adopts the (reverse) bias' of the connected cells
         */
        public Boolean getAdoptBias() {
            return adoptBias;
        }

        public void setAdoptBias(Boolean adoptBias) {
            if (this.adoptBias != adoptBias) {
                this.adoptBias = adoptBias;
//                if (adoptBias) {
//                    this.collectAndAdoptBias();
//                }
            }
        }

        public basicAnalog getCell() {
            return _stressCell;
        }

        public boolean isBackPropagating() {
            return backPropagating;
        }

        public void setBackPropagating(boolean backPropagating) {
            if (backPropagating != this.backPropagating) {
                this.backPropagating = backPropagating;
                _stressCell.setBackPropagating(this.backPropagating);
            }
        }

        public void receivePulse(Pulse pulse) {
            if (cellStressor) {
                //tress affects the triggerlevel of the cell inversely
                Double origTriggerLEvel = _associatedCell.getTriggerLevel();
                Double newTriggerLevel = origTriggerLEvel * (1 / pulse.intensity);
                _associatedCell.setTriggerLevel(newTriggerLevel);
                logger.finest(_associatedCell.getName() + " triggerLevel = " + newTriggerLevel);
            } else {
                //stress affects the connection bias of the connection it traveled over

                //find the bias
                cellClass toCell;
                cellClass fromCell;

                //alter the bias
                AllCells allCells = AllCells.getInstance();
                List<String> connected;

                cellClass[] c = new cellClass[2];
                if (backPropagating) {
                    c[0] = _associatedCell;
                    c[1] = allStressors.get(pulse.emittingID).getAssociated();
                } else {
                    c[1] = _associatedCell;
                    c[0] = allStressors.get(pulse.emittingID).getAssociated();
                }

                connected = allCells.carriers().connectedFromNames(c[1].getName());
                if (connected.contains(c[0].getName())) {
                    Double orgBias = c[0].getConnectionBias(c[1].getName());
                    Double newBias = orgBias * (1 / pulse.intensity);
                    c[1].setConnectionBias(c[0].getName(), newBias);
                } else {
                    logger.info(c[1].getName() + "->" + c[0].getName() + " is not connected");
                }

            }


        }

        @Override
        public String toString() {
            return toJson().toString();
        }

        public cellClass getAssociated() {
            return _associatedCell;
        }

        public void disconnect() {
            _stressCell.disconnect();
        }


    }

    /**
     * A cell class that emits a pulse at set intervals
     */
    public static class heartbeat extends  cellClass {

        private beatThreadClass beatThread = new beatThreadClass(this);

        /**
         * Cell with default settings, name defaults to the object hashID
         */
        public heartbeat() {
            super();
            init();
        }

        private void init() {
        }

        /**
         * @param cellName name of the cell
         */
        public heartbeat(String cellName) {
            super(cellName);
            init();
        }

        /**
         * load the cell properties, including connections and bias, from a json object.
         *
         * @param jsonObject a json object containing the needed info
         */
        public heartbeat(JsonObject jsonObject) {
            super(jsonObject);
            init();
        }

        /**
         * load the cell properties, including connections and bias, from a json object.
         *
         * @param jsonObject  a json object containing the needed info
         * @param connectBias should bias' loaded from the json be registered as connections too
         */
        public heartbeat(JsonObject jsonObject, boolean connectBias) {
            super(jsonObject, connectBias);
            init();
        }

        public void start() {
            if (newBeatThread()) {
                beatThread.start();
            }
        }

        /**
         * create a thread that:
         * - emits a pulse
         * - waits for an interval, based on the triggerlevel, without blocking other threads
         * - starts over
         */
        private boolean newBeatThread() {
            if (beatThread.running) {
                return false;
            }
            beatThread = new beatThreadClass(this);
            beatThread.setName("heartbeat " + getName());
            return true;
        }

        public void stop() {
            beatThread.running = false;
            try {
                beatThread.join(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /** A thread that:
         * - emits a pulse
         * - waits for an interval, based on the triggerlevel, without blocking other threads
         * - starts over
         */
        private class beatThreadClass extends Thread {
            Boolean running = false;
            private Logger logger = Logger.getLogger(getClass().getName());
            private heartbeat parent;
            public beatThreadClass(heartbeat parentInstance) {
                parent = parentInstance;
            }

            /**
             * When an object implementing interface <code>Runnable</code> is used
             * to create a thread, starting the thread causes the object's
             * <code>run</code> method to be called in that separately executing
             * thread.
             * <p>
             * The general contract of the method <code>run</code> is that it may
             * take any action whatsoever.
             *
             * @see Thread#run()
             */
            @Override
            public void run() {
                running = true;
                Long beatTime;
                long startTime;
                while (!parent.getDeactivated() && running) {
                    beatTime = ((Double) (parent.getTriggerLevel() * 1000d)).longValue();
                    startTime = System.currentTimeMillis();
                    logger.finest("beat " + parent.getName() + " every " + beatTime + "ms");
                    parent.sendPulse(1d);
                    while (!parent.getDeactivated() && running) {
                        if (System.currentTimeMillis() - startTime >= beatTime) {
                            break;
                        }
                        Thread.yield();
                        try {
                            Thread.sleep(beatTime/10);
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                }
                running = false;
            }
        }

        /**
         * Process a pulse:
         * Set the intensity to the given pulse
         *
         * @param pulse recieved pulse
         */
        @Override
        public void receivePulse(Pulse pulse) {
            setTriggerLevel(1d/pulse.intensity);
            if (!beatThread.running) {
                newBeatThread();
                beatThread.start();
            }
            if (cellStressor != null) {
                cellStressor.receivePulse(pulse);
            }
        }
    }

}
