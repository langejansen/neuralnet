package com.langejansen.net;

import com.langejansen.net.support.AllStressors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by tim on 29-4-17.
 */
public class StressorCollection {
    private HashMap<String, cellClass.stressor> _stressorFor = new HashMap<>();
    private HashMap<String, cellClass.stressor> _stressors = new HashMap<>();

    public StressorCollection() {
    }

    public void add(cellClass.stressor newStressor) {
        _stressors.put(newStressor.getCell().getName(), newStressor);
        _stressorFor.put(newStressor.getAssociated().getName(), newStressor);
    }

    public cellClass.stressor get(String name) {
        return _stressors.get(name);
    }

    public List<cellClass.stressor> all() {
        List<cellClass.stressor> retList = new ArrayList<>();
        retList.addAll(_stressors.values());
        return retList;
    }

    public cellClass.stressor getStressorFor(String cellName) {
        return _stressorFor.getOrDefault(cellName, null);
    }

    public void adoptAssociated() {
        for (cellClass.stressor theStressor : all()) {
            theStressor.disconnect();
        }

        for (cellClass.stressor theStressor : all()) {
            theStressor.collectAndAdoptBias();
        }
    }

    public void relax() {
        for (cellClass.stressor stressor : all()) {
            stressor.getCell().setPotential(0d);
        }
    }

    //todo: stressors to json

    public static class propagating extends StressorCollection {
        Boolean backPropagating = false;
        CellCollection targetNet;
        private Logger logger = Logger.getLogger(getClass().getName());

        public propagating(CellCollection targetNet, boolean backpropagating) {
            backPropagating = backpropagating;
            this.targetNet = targetNet;
            laynet();
        }

        public propagating(CellCollection targetNet) {
            backPropagating = false;
            this.targetNet = targetNet;
            laynet();
        }

        /**
         * Add this stressor net over the targetNet
         */
        //todo: add test for laynet
        private void laynet() {
            logger.fine("laying net");
            logger.finer("  + stressors");
            AllStressors allStressors = AllStressors.getInstance();
            for (cellClass cell : targetNet.getAll()) {
                cellClass.stressor newStressor = new cellClass.stressor(cell);
                logger.finest("\tStressor created: " + newStressor.getCell().getName());
                //todo: backpropagating of stressor into json
                newStressor.setBackPropagating(backPropagating);
                newStressor.setAdoptBias(true);
                logger.finest("\tStressor created: " + newStressor.getCell().getName());
            }
            logger.finer("  /+ stressors");
            logger.finer("  connecting");
            allStressors.adoptAssociated();
            logger.finer("  /connecting");
            logger.fine("/laying net");
        }

    }

}
