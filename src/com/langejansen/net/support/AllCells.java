package com.langejansen.net.support;

import com.langejansen.net.CarrierCollection;
import com.langejansen.net.CellCollection;

/**
 * Created by tim on 21-4-17.
 */
public class AllCells extends CellCollection {
    private static CarrierCollection _carriers = null;
    private static AllCells ourInstance = null;

    private AllCells() {
    }

    public static AllCells getInstance() {
        if (ourInstance == null) {
            ourInstance = new AllCells();
        }
        return ourInstance;
    }

    /**
     * @return singlet instance
     */
    @Override
    public CarrierCollection carriers() {
        if (_carriers == null) {
            _carriers = new CarrierCollection();
        }
        return _carriers;
    }

}
