package com.langejansen.net.support;

import com.langejansen.net.StressorCollection;

/**
 * Created by tim on 21-4-17.
 */
public class AllStressors extends StressorCollection {
    private static AllStressors ourInstance = new AllStressors();

    private AllStressors() {
        super();
    }

    public static AllStressors getInstance() {
        return ourInstance;
    }
}
