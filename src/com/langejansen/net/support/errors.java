package com.langejansen.net.support;

/**
 * Created by tim on 23-4-17.
 */
public abstract class errors {
    public static class cellNotFoundError extends Exception {
        private String cellName;

        /**
         * Constructs a new exception with {@code null} as its detail message.
         * The cause is not initialized, and may subsequently be initialized by a
         * call to {@link #initCause}.
         */
        public cellNotFoundError(String cellName) {
            super();
            this.cellName = cellName;
        }

        /**
         * Returns the detail message string of this throwable.
         *
         * @return the detail message string of this {@code Throwable} instance
         * (which may be {@code null}).
         */
        @Override
        public String getMessage() {
            return "cell '" + cellName + "' not found";
        }

        /**
         * Creates a localized description of this throwable.
         * Subclasses may override this method in order to produce a
         * locale-specific message.  For subclasses that do not override this
         * method, the default implementation returns the same result as
         * {@code getMessage()}.
         *
         * @return The localized description of this throwable.
         * @since JDK1.1
         */
        @Override
        public String getLocalizedMessage() {
            return super.getLocalizedMessage();
        }
    }

    public static class notConnectedError extends Throwable {

        public notConnectedError(String from, String to) {
            super(from + " not connected to " + to);
        }

    }
}
