package com.langejansen.net.support;

import com.langejansen.net.CellCollection;
import com.langejansen.net.cellClass;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by tim on 2-5-17.
 */
public abstract class netConstructor {
    private int inputs;
    private int outputs;

    public abstract CellCollection create();

    public Integer getInputs() {
        return inputs;
    }

    public void setInputs(int inputs) {
        this.inputs = inputs;
    }

    public Integer getOutputs() {
        return outputs;
    }

    public void setOutputs(int outputs) {
        this.outputs = outputs;
    }

    public static class uniform extends netConstructor {
        private final Long size;
        private final Double triggerLEvel;
        private final Double initialBias;
        private final Long connections;
        public boolean nameInputs;
        private CellCollection collection;

        public uniform(Long size, Double triggerLevel, Double initialBias, Long connections) {
            this.size = size;
            triggerLEvel = triggerLevel;
            this.initialBias = initialBias;
            this.connections = connections;
            nameInputs = false;
        }

        @Override
        public CellCollection create() {
            collection = new CellCollection();

            //layer it up
            /**make first layer
             *
             *
             **/
            cellClass[] firstLayer = new cellClass[connections.intValue()];
            cellClass[] prevLayer;
            cellClass[] curLayer = new cellClass[connections.intValue()];

            for (Integer i = 0; i < connections; i++) {
                if (nameInputs) {
                    firstLayer[i] = collection.addCell("0." + i);
                } else {
                    firstLayer[i] = collection.addCell();
                }
                firstLayer[i].setTriggerLevel(triggerLEvel);
            }
            Logger logger = Logger.getLogger(getClass().getName());


            Integer count = 0;
            Integer layerCount = 0;
            prevLayer = firstLayer;

            while (count < size) {
                layerCount++;
                for (int i = 0; i < connections; i++) {
                    count++;
                    if (count > size) {
                        break;
                    }

                    logger.info("cell " + layerCount + "." + count);
                    if (nameInputs) {
                        curLayer[i] = collection.addCell(layerCount + "." + i);
                    } else {
                        curLayer[i] = collection.addCell();
                    }

                    curLayer[i].setTriggerLevel(triggerLEvel);
                    for (int n = 0; n < connections; n++) {
//                        curLayer[i].connectCell(prevLayer[n]);
                        prevLayer[n].connectCell(curLayer[i]);
                        try {
                            collection.carriers().setBias(prevLayer[n].getName(), curLayer[i].getName(), initialBias);
                        } catch (errors.notConnectedError notConnectedError) {
                            logger.warning(notConnectedError.getMessage());
                            //ignore this
                        }
                    }
                }
                prevLayer = curLayer;
                curLayer = new cellClass[connections.intValue()];
            }

            if (getInputs() != null) {
                List<cellClass> collectionCells = collection.getAll();
                for (int i = 0; i < getInputs(); i++) {
                    collectionCells.get(i).setInput(i);
                    logger.info("set as Input:\t" + collectionCells.get(i).getName());
                }
            }

            if (getOutputs() != null) {
                List<cellClass> collectionCells = collection.getAll();
                for (int i = 0; i < getOutputs(); i++) {
                    collectionCells.get(collectionCells.size() - i - 1).setOutput(i);
                    logger.info("set as output:\t" + collectionCells.get(collectionCells.size() - i - 1).getName());
                }

            }
            collection.refreshInputsOutputs();

            return collection;
        }

        public CellCollection createRandom() {
            collection = new CellCollection();

            for (Long i = 0l; i < size; i++) {
                collection.addCell().setTriggerLevel(triggerLEvel);
            }

            for (cellClass cell : collection.getAll()) {
                while (collection.carriers().connectedToNames(cell.getName()).size() < connections) {
                    cellClass toCell = collection.randomCell();
                    cell.connectCell(toCell);
                    try {
                        collection.carriers().setBias(cell.getName(), toCell.getName(), initialBias);
                    } catch (errors.notConnectedError notConnectedError) {
                        //ignore this
                    }
                }
            }

            if (getInputs() != null) {
                for (int i = 0; i < getInputs(); i++) {
                    collection.randomCell().setInput(i);
                }
            }

            if (getOutputs() != null) {
                for (int i = 0; i < getOutputs(); i++) {
                    collection.randomCell().setOutput(i);
                }
            }

            return collection;
        }

    }
}
