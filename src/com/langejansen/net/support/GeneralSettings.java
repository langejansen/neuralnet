package com.langejansen.net.support;

import java.util.logging.*;

/**
 * Created by tim on 25-4-17.
 */
public class GeneralSettings {
    private static GeneralSettings ourInstance = new GeneralSettings();
    public Logger logger = Logger.getLogger("com.langejansen");
    Handler testHandler = new Handler() {
        @Override
        public void publish(LogRecord record) {
            System.out.print(getFormatter().format(record));
        }

        @Override
        public void flush() {
            System.out.flush();
            System.err.flush();
        }

        @Override
        public void close() throws SecurityException {
        }
    };
    Formatter logFormatter = new Formatter() {
        private String buildStackPath() {
            String stackPath = "";
            for (StackTraceElement elem : Thread.currentThread().getStackTrace()) {
                String elemClassName = elem.getClassName();
                if (elemClassName.startsWith(getClass().getPackage().getName())) {
                    String strippedName = elemClassName.replace(getClass().getPackage().getName() + ".", "");
                    if (!strippedName.startsWith(getClass().getSimpleName())) {
                        stackPath = strippedName + "." + elem.getMethodName() + ":" + elem.getLineNumber() + "\n" + stackPath;
                    }
                }
            }


            return stackPath;
        }

        @Override
        public String format(LogRecord record) {
            String retString = "";
            retString += record.getLevel() + "\t" + "[" + Thread.currentThread().getName() + "]" + record.getSourceClassName().toString().replace("com.langejansen.", "") + "->" + record.getSourceMethodName() + " : \t" + record.getMessage() + "\n";
//            retString += "\t\t" + buildStackPath().replaceAll("\n", "\n\t\t") + "\n";
            return retString;
        }
    };

    private GeneralSettings() {
        //one time setup
        testHandler.setFormatter(logFormatter);
        logger.setUseParentHandlers(false);

        logger.addHandler(testHandler);
        logger.setLevel(Level.INFO);
    }

    public static GeneralSettings getInstance() {
        return ourInstance;
    }

    public void generalInit() {
        return;
    }
}
