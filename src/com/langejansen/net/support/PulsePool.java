package com.langejansen.net.support;

import com.langejansen.net.Pulse;
import com.langejansen.net.cellClass;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Created by tim on 20-4-17.
 */
public abstract class PulsePool implements Runnable{
    private Logger logger = Logger.getLogger(getClass().getName());
    private int _maxActivePulses = 200;
    public int getMaxActivePulses() {
        return _maxActivePulses;
    }
    public void setMaxActivePulses(int maxActivePulses) {
        _maxActivePulses = maxActivePulses;
    }

    public abstract void stop();
    public abstract void add(Pulse pulse);
    public abstract void winddown(long timeoutMillis);
    public abstract void shutdown();

    /**
     * Let the pulses/cells run for a fixed time
     *
     * @param timeSeconds time to run the net in seconds
     */
    public Long runfor(Long timeSeconds) {
        logger.fine("run for " + timeSeconds + " seconds");
        logger.info("\t 0\tsec " + getActivePulses() + " pulses, " + getQueuedPulses() + " queued");
        long minRunTime = timeSeconds * 1000;
        long startTime = System.currentTimeMillis();
        long lastChecked = startTime;
        long totalRunTime = 0;
        while (true) {
            totalRunTime = System.currentTimeMillis() - startTime;

            if (!anyRunning()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    break;
                }
                if (!anyRunning()) {
                    break;
                }
            }

            if (totalRunTime >= minRunTime) {
                break;
            }
            if (System.currentTimeMillis() - lastChecked >= 10000) {
                Long runTimeS = (System.currentTimeMillis() - startTime) / 1000;
                String runTimeStr = ((Integer) runTimeS.intValue()).toString();
                logger.info("\t " + runTimeStr + "\tsec " + getActivePulses() + " pulses, " + getQueuedPulses() + " queued");
                lastChecked = System.currentTimeMillis();
            }
//            Thread.yield();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
        }
        logger.fine("/run for " + timeSeconds + " seconds");
        return totalRunTime;
    }

    public abstract Boolean anyRunning();

    public abstract int getActivePulses();

    public abstract int getQueuedPulses();

    /**
     * runnable class to thread a pulse
     */
    public static class pulseQueueThread implements Runnable {
        public volatile Boolean running = false;
        public Thread thread;
        Pulse pulse;
        List<cellClass> recipients;
        private Logger logger = Logger.getLogger(getClass().getName());

        public pulseQueueThread(Pulse pulse) {
            init();
            this.pulse = pulse;
            recipients = pulse.getRecipients();
        }

        private void init() {
            recipients = new ArrayList<>();
            thread = new Thread(this);
        }

        public void start() {
            thread.start();
        }

        @Override
        public void run() {
            logger.finest("Thread started for " + recipients.size() + " recipients");
            running = true;
            cellClass targetCell;
            while (recipients.size() > 0 && running) {
                targetCell = recipients.get(0);
                recipients.remove(0);
                if (pulse.push) {
                    targetCell.receivePulse(pulse);
                } else {
                    Double pulseTotal = 0d;
                    if (!targetCell.incomingPulsesClaimed()) {
                        List<Pulse> incomingPulses = targetCell.getIncomingPulses();
                        targetCell.setIncomingPulsesClaimed(true);
                        while (incomingPulses.size() > 0) {
                            try {
                                Pulse incomingPulse = incomingPulses.get(0);
                                incomingPulses.remove(0);
                                pulseTotal += incomingPulse.intensity;
                            } catch (IndexOutOfBoundsException E) {
                                //ignore this
                            } catch (NullPointerException N) {
                                //ignore this too
                            }
                        }
                        if (pulseTotal > 0d) {
                            //make the target recieve the pulses
                            targetCell.receivePulse(new Pulse.basic(pulse.emittingID, pulseTotal));
                        }
                        targetCell.setIncomingPulsesClaimed(false);
                    }
                }
            }
            running = false;
            logger.finest("done");
        }
    }

    /**
     * Uses an executorService to manage the pool
     */
    public static class executor extends PulsePool {
        private Logger logger = Logger.getLogger(getClass().getName());

        //Get the ThreadFactory implementation to use
//        private ThreadFactory threadFactory = Executors.defaultThreadFactory();
        private ThreadFactory threadFactory = Executors.defaultThreadFactory();
        private ExecutorService pulsePool = Executors.newFixedThreadPool(getMaxActivePulses(), threadFactory);
        //        private ExecutorService pulsePool = Executors.newFixedThreadPool(getMaxActivePulses());
        private Boolean shuttingDown = false;

        @Override
        public void setMaxActivePulses(int maxActivePulses) {
            pulsePool.shutdown();
            while (!pulsePool.isShutdown()) {
            }
            super.setMaxActivePulses(maxActivePulses);
            pulsePool = Executors.newFixedThreadPool(getMaxActivePulses());
        }

        /**
         * signal queue monitor to stop
         */
        @Override
        public void stop() {
            pulsePool.shutdown();
        }

        /**
         * Add a pulse to the queue
         * @param pulse pulse element
         */
        @Override
        public void add(Pulse pulse) {
            if (shuttingDown == true || pulsePool.isShutdown()) {
                return;
            }
            try {
                pulsePool.execute(new PulsePool.pulseQueueThread(pulse));
            } catch (RejectedExecutionException e) {
                logger.warning("this shouldn't happen:" + e.getLocalizedMessage());
            }
            logger.finest("Added " + pulse + " to the queue (queue hash " + hashCode() + ")");
        }

        /**
         * Waits for an inactive state and shuts down
         *
         * @param timeoutMillis timeout in milliseconds
         */
        @Override
        public void winddown(long timeoutMillis) {
            winddown(timeoutMillis, 1000);
        }

        /**
         * Waits for an inactive state and shuts down
         *
         * @param timeoutMillis timeout in milliseconds
         * @param minRunTime    the minimum time the pulses need to get to run their coarse.
         */
        public void winddown(long timeoutMillis, long minRunTime) {
            logger.fine("Winding down");
            runfor(minRunTime / 1000);
            try {
                pulsePool.awaitTermination(timeoutMillis, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            shutdown();
            logger.fine("/Winding down");
        }

        /**
         * tell all the runners we're not running anymore and close the threads.
         */
        @Override
        public void shutdown() {
            logger.info("shutdown");
            shuttingDown = true;
            pulsePool.shutdown();
            while (!pulsePool.isShutdown()) {
            }
            shuttingDown = false;
            pulsePool = Executors.newFixedThreadPool(getMaxActivePulses());
            logger.info("/shutdown");
        }

        /**
         * @return are any pulses active?
         */
        @Override
        public Boolean anyRunning() {
            int threadQ = ((ThreadPoolExecutor) pulsePool).getQueue().size();
            return !(pulsePool.isTerminated() || pulsePool.isShutdown() || getActivePulses() == 0);
        }

        @Override
        public int getActivePulses() {
            return ((ThreadPoolExecutor) pulsePool).getActiveCount();
        }

        @Override
        public int getQueuedPulses() {
            return ((ThreadPoolExecutor) pulsePool).getQueue().size();
        }

        /**
         * When an object implementing interface <code>Runnable</code> is used
         * to create a thread, starting the thread causes the object's
         * <code>run</code> method to be called in that separately executing
         * thread.
         * <p>
         * The general contract of the method <code>run</code> is that it may
         * take any action whatsoever.
         *
         * @see Thread#run()
         */
        @Override
        public void run() {
            
        }


    }

}
