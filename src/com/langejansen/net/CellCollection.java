package com.langejansen.net;

import com.google.gson.*;
import com.langejansen.net.support.AllCells;
import com.langejansen.net.support.AllStressors;
import com.langejansen.net.support.errors;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by tim on 15-4-17.
 */
public class CellCollection {
    private List<cellClass> _cells = new ArrayList<>();
    private HashMap<String, cellClass> _cellsByName = new HashMap<>();
    private Logger logger = Logger.getLogger(getClass().getName());
    private HashMap<Integer, String> _inputs = new HashMap<>();
    private HashMap<Integer, String> _outputs = new HashMap<>();
    private cellClass inputPoint = null;
    private cellClass stressorPoint = null;

    public CellCollection() {

    }

    public HashMap<Integer, String> getOutputs() {
        return _outputs;
    }

    public HashMap<Integer, cellClass> getOutputCells() {
        AllCells allCells = AllCells.getInstance();
        HashMap<Integer, cellClass> retMap = new HashMap<>();
        for (Integer outputIndex : _outputs.keySet()) {
            retMap.put(outputIndex, allCells.getCell(_outputs.get(outputIndex)));
        }

        return retMap;
    }

    public HashMap<Integer, cellClass> getOutputStressorCells() {
        HashMap<Integer, cellClass> retMap = new HashMap<>();
        AllStressors allStressors = AllStressors.getInstance();
        for (Integer outputIndex : _outputs.keySet()) {
            retMap.put(outputIndex, allStressors.getStressorFor(_outputs.get(outputIndex)).getCell());
        }
        return retMap;
    }

    public HashMap<Integer, String> getOutputStressors() {
        HashMap<Integer, String> retMap = new HashMap<>();
        AllStressors allStressors = AllStressors.getInstance();
        HashMap<Integer, cellClass> outputStressorCells = getOutputStressorCells();
        for (Integer outputIndex : outputStressorCells.keySet()) {
            retMap.put(outputIndex, outputStressorCells.get(outputIndex).getName());
        }
        return retMap;
    }

    public HashMap<Integer, String> getInputs() {
        return _inputs;
    }

    public HashMap<Integer, cellClass> getInputCells() {
        AllCells allCells = AllCells.getInstance();
        HashMap<Integer, cellClass> retMap = new HashMap<>();
        for (Integer inputIndex : _inputs.keySet()) {
            retMap.put(inputIndex, allCells.getCell(_inputs.get(inputIndex)));
        }
        return retMap;
    }

    public void clear() {
        for (cellClass cell: _cells){
            cell.disconnect();
            cell.deactivate();
        }
        _cells.clear();
        _cellsByName.clear();
    }


    public void removeCell(cellClass targetCell) {
        targetCell.deactivate();
        carriers().disconnect(targetCell);
        _cells.remove(targetCell);
        _cellsByName.remove(targetCell.getName());
    }


    /**
     * @return singlet instance
     */
    public CarrierCollection carriers() {
        return AllCells.getInstance().carriers();
    }

    /**
     * Add a new cell to the collective
     * @return a new cell
     */
    public cellClass addCell() {
        return addCell(new cellClass.basic());
    }

    /**
     * Add a cell to the collective from a Json object
     * @param cellJson json object with the cell properties
     */
    public cellClass addCell(JsonObject cellJson) {
        return addCell(cellJson, true);
    }

    /**
     * Add a cell to the collective from a Json object
     * @param cellJson json object with the cell properties
     */
    public cellClass addCell(JsonObject cellJson, boolean connectBias) {
        Class t;
        Constructor<?> constructor = null;
        String typeName = "basic";
        if (cellJson.has("type")) {
            typeName = cellJson.get("type").getAsString();
        }
        String cellClassName = cellClass.class.getName();
        cellClass newCell = null;
        try {
            t =  Class.forName(cellClassName + "$" + typeName);
            constructor = t.getConstructor(JsonObject.class, boolean.class);
            newCell = (cellClass) constructor.newInstance(cellJson, connectBias);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            logger.warning("Unknown cell Type: " + typeName + " not found as a subclass of " + cellClassName);
            e.printStackTrace();
        }
        return newCell;
    }

    /**
     * Add a cell to the collective
     * @param newCell cellClass instance that will be added
     * @return the added cell
     */
    public cellClass addCell(cellClass newCell) {
        return addCell(newCell, true);
    }

    /**
     * Add a cell to the collective
     * @param newCell cellClass instance that will be added
     * @param replace replace any existing?
     * @return the added cell
     */
    public cellClass addCell(cellClass newCell, boolean replace) {
        if (!_cellsByName.containsKey(newCell.getName())) {
            _cells.add(newCell);
            _cellsByName.put(newCell.getName(),newCell);
            if (newCell.isInput() != -1) {
                _inputs.put(newCell.isInput(), newCell.getName());
            }
            if (newCell.isOutput() != -1) {
                _outputs.put(newCell.isOutput(), newCell.getName());
            }
        } else {
            cellClass oldCell = _cellsByName.get(newCell.getName());
            if (!oldCell.equals(newCell)) {
                oldCell.deactivate();
                _cells.remove(oldCell);
                _cellsByName.remove(newCell.getName());
                addCell(newCell);
            }
        }
        return newCell;
    }

    public void refreshInputsOutputs() {
        _inputs.clear();
        _outputs.clear();

        for (cellClass cell : _cells) {
            if (cell.isInput() != -1) {
                _inputs.put(cell.isInput(), cell.getName());
            }
            if (cell.isOutput() != -1) {
                _outputs.put(cell.isOutput(), cell.getName());
            }
        }

    }


    public List<cellClass> getAll() {
        return _cells;
    }

    /**
     * Get a cell by Name
     * @param cellName cell name
     * @return found cell
     */
    public cellClass getCell(String cellName) {
        if (_cellsByName.containsKey(cellName)) {
            return _cellsByName.get(cellName);
        } else {
            logger.warning("Cell not found: " + cellName);
        }
        return null;
    }

    /**
     * @return JSON formatted string of this instance
     */
    public JsonArray toJSON() {
        Gson gson = new Gson();
        JsonArray j = new JsonArray();
        for (cellClass cell: _cells) {
            j.add(cell.toJSON());
        }
        return j;
    }

    /**
     * @param theJson jsonArray with cell data
     */
    public void fromJson(JsonArray theJson) {
        for (JsonElement jElement: theJson) {
            JsonObject jObj = jElement.getAsJsonObject();

            cellClass newCell = addCell(jObj, false);
            logger.fine("Loaded cell " + newCell.getName());
        }
        biasToConnections();
    }

    public void fromJson(String jsonSource) {

        Gson gson = new Gson();
        fromJson(gson.fromJson(jsonSource,JsonArray.class));
    }

    private void biasToConnections() {
        for (cellClass theCell: _cells) {
            carriers().biasToConnections(theCell);
        }
    }

    public void save(String fileName) {
        JsonArray brainJson = toJSON();
        FileWriter writer = null;
        try {
            writer = new FileWriter(fileName);
            //use gson for prettyPrinting.
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            writer.write(gson.toJson(brainJson));
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load(String fileName) {
        Gson gson = new Gson();
        try {
            FileReader fr = new FileReader(fileName);
            Reader br = new BufferedReader(fr);
            //convert the json string back to object
            fromJson(gson.fromJson(br,JsonArray.class));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Start all the heartbeats
     */
    public void startBeats() {
        for (cellClass cell: _cells) {
            if (cell.getClass() == cellClass.heartbeat.class) {
                cellClass.heartbeat heart = (cellClass.heartbeat) cell;
                heart.start();
            }
        }
    }

    /**
     * Stop all the heartbeats
     */
    public void stopBeats() {
        for (cellClass cell: _cells) {
            if (cell.getClass() == cellClass.heartbeat.class) {
                cellClass.heartbeat heart = (cellClass.heartbeat) cell;
                heart.stop();
            }
        }
    }

    private void initStressorPoint() {
        initStressorPoint(false);
    }

    private void initStressorPoint(boolean force) {
        if (stressorPoint == null || force) {
            stressorPoint = addCell(new cellClass.basic("stressorPoint"));
            if (force) {
                stressorPoint.disconnect();
            }
            HashMap<Integer, String> stressors = getOutputStressors();
            for (Integer stressorCellIndex : stressors.keySet()) {
                String stressorCellName = stressors.get(stressorCellIndex);
                logger.fine("Unknown stressor " + stressorCellName + " with index " + stressorCellIndex + " added");
                carriers().connect("stressorPoint", stressorCellName);
            }
        }
    }

    public void applyStress(Double[] stressValues, Double stressLevel) {
        //make the stress emit from a basic point/cell so the
        // pulses start at the same time (as much as possible)
        initStressorPoint();
        HashMap<Integer, String> stressors = getOutputStressors();
        for (Integer stressorCellIndex : stressors.keySet()) {
            try {
                carriers().setBias("stressorPoint", stressors.get(stressorCellIndex), stressValues[stressorCellIndex]);
            } catch (errors.notConnectedError notConnectedError) {
                logger.severe("unknown stressor found. refreshing inputs");
                initStressorPoint(true);
                //do this again
                applyStress(stressValues, stressLevel);
                return;
            }
        }
        stressorPoint.sendPulse(stressLevel);
    }

    private void initInputPoint() {
        initInputPoint(false);
    }

    private void initInputPoint(boolean force) {
        if (inputPoint == null || force) {
            inputPoint = addCell(new cellClass.basic("inputPoint"));
            if (force) {
                inputPoint.disconnect();
            }
            HashMap<Integer, String> inputs = getInputs();
            for (Integer inputCellIndex : inputs.keySet()) {
                String inputCellName = inputs.get(inputCellIndex);
                logger.info("Unknown input " + inputCellName + " with index " + inputCellIndex + "added");
                if (!_cellsByName.containsKey(inputCellName)) {
                    cellClass newInput = addCell(new cellClass.basic(inputCellName));
                    newInput.setInput(inputCellIndex);
                }
                carriers().connect("inputPoint", inputCellName);
            }
        }
    }

    //todo: test applinput/getoutputarray
    public void applyInput(Double[] inputs) {
        //make the input emit from a basic point/cell so the pulses start at the same time (as much as possible)
        initInputPoint();
        HashMap<Integer, String> inputNames = getInputs();
        for (Integer inputIndex : inputNames.keySet()) {
            try {
                carriers().setBias("inputPoint", inputNames.get(inputIndex), inputs[inputIndex]);
            } catch (errors.notConnectedError notConnectedError) {
                logger.severe("unknown input found. refreshing inputs");
                initInputPoint(true);
                //do this again
                applyInput(inputs);
                return;
            }
        }
        inputPoint.sendPulse(1d);
    }

    public Double[] getOutputArray() {
        HashMap<Integer, Double> outputVals = getOutputValues();
        int maxIndex = max(outputVals.keySet().toArray(new Integer[outputVals.size()]));
        Double[] retValues = new Double[maxIndex];
        for (int retValueIndex : outputVals.keySet()) {
            retValues[retValueIndex] = outputVals.get(retValueIndex);
        }
        return retValues;
    }

    public HashMap<Integer, Double> getOutputValues() {
        HashMap<Integer, Double> retValues = new HashMap<>();
        HashMap<Integer, String> outputs = getOutputs();
        for (int retValueIndex : outputs.keySet()) {
            retValues.put(retValueIndex, getCell(outputs.get(retValueIndex)).getPotential());
        }
        return retValues;
    }

    private Double max(Double[] values) {
        Double retVal = Double.MIN_VALUE;
        for (Double value : values) {
            retVal = Math.max(retVal, value);
        }
        return retVal;
    }

    private Integer max(Integer[] values) {
        Integer retVal = Integer.MIN_VALUE;
        for (Integer value : values) {
            retVal = Math.max(retVal, value);
        }
        return retVal;
    }

    public void relax() {
        for (cellClass cell : _cells) {
            cell.setPotential(0d);
        }
    }

    public cellClass randomCell() {
        int randInt = ((Double) (Math.random() * ((Integer) _cells.size()).doubleValue())).intValue();
        return _cells.get(randInt);
    }

    public cellClass addCell(String cellName) {
        return addCell(new cellClass.basic(cellName));
    }

    public List<cellClass> getCells(String nameRegex) {
        List<cellClass> retList = new ArrayList<>();
        for (String cellName : _cellsByName.keySet()) {
            if (cellName.matches(nameRegex)) {
                retList.add(_cellsByName.get(cellName));
            }

        }
        return retList;
    }
}
