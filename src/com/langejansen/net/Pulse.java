package com.langejansen.net;

import com.langejansen.net.support.AllCells;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by tim on 15-4-17.
 */
public abstract class Pulse {
    public boolean push = true;
    public String emittingID;
    public Double intensity;
    private cellClass emittingCell = null;
    private List<cellClass> recipients;
    private boolean _reverse = false;

    /**
     * @param originatingID id of the emiting cells
     * @param intensity intensity of the pulse
     */
    public Pulse(String originatingID, Double intensity) {
        emittingID = originatingID;
        this.intensity = intensity;
        Logger logger = Logger.getLogger(getClass().getName());
        logger.finest("Created pulse: " + originatingID);
    }

    public void setReverse(boolean _reverse) {
        this._reverse = _reverse;
    }

    public boolean reverse() {
        return _reverse;
    }

    public Double getBias(cellClass recievingCell, Double defaultVal) {
        if (_reverse) {
            return recievingCell.getConnectionBias(emittingID, defaultVal);
        } else {
            return emittingCell().getConnectionBias(recievingCell.getName(), defaultVal);
        }
    }

    public Double getBias(cellClass recievingCell) {
        if (_reverse) {
            return recievingCell.getConnectionBias(emittingID);
        } else {
            return emittingCell().getConnectionBias(recievingCell.getName());
        }
    }

    @Override
    public String toString() {
        return "{Pulse: '"+ emittingID +"', Intensity : "+ intensity + "}";
    }

    public List<cellClass> getRecipients() {
        if (!_reverse) {
            return AllCells.getInstance().carriers().connectedTo(emittingID);
        } else {
            return AllCells.getInstance().carriers().connectedFrom(emittingID);
        }
    }

    public cellClass emittingCell() {
        if (emittingCell == null) {
            emittingCell = AllCells.getInstance().getCell(emittingID);
        }
        return emittingCell;
    }

    //    public static class pushmodel extends Pulse {
//        /**
//         * @param originatingID id of the emiting cells
//         * @param intensity     intensity of the pulse
//         */
//        public pushmodel(String originatingID, Double intensity) {
//            super(originatingID, intensity);
//        }
//
//        public static class backpropagating extends Pulse.pushmodel {
//
//            /**
//             * @param originatingID id of the emiting cells
//             * @param intensity     intensity of the pulse
//             */
//            public backpropagating(String originatingID, Double intensity) {
//                super(originatingID, intensity);
//                super.setReverse(true);
//            }
//        }
//
//    }
//
    public static class pullmodel extends Pulse {
        private List<cellClass> recipients = null;

        /**
         * @param originatingID id of the emiting cells
         * @param intensity     intensity of the pulse
         */
        public pullmodel(String originatingID, Double intensity) {
            super(originatingID, intensity);
            push = false;
            recipients = super.getRecipients();
            boolean retry = true;
            for (cellClass recipient : recipients) {
                List<Pulse> recipentIncomingPulses = recipient.getIncomingPulses();
                retry = true;
                while (retry) {
                    try {
                        recipentIncomingPulses.add(this);
                        retry = false;
                    } catch (ArrayIndexOutOfBoundsException e) {
//                        e.printStackTrace();
                        retry = true;
                    }
                }
            }
        }

        @Override
        public List<cellClass> getRecipients() {
            if (recipients == null) {
                recipients = super.getRecipients();
            }
            return recipients;
        }

        public static class backpropagating extends Pulse.pullmodel {
            /**
             * @param originatingID id of the emiting cells
             * @param intensity     intensity of the pulse
             */
            public backpropagating(String originatingID, Double intensity) {
                super(originatingID, intensity);
                super.setReverse(true);
            }
        }

    }

    public static class basic extends Pulse {
        public basic(String originatingID, Double intensity) {
            super(originatingID, intensity);
        }
    }
}
