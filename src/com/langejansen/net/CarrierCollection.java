package com.langejansen.net;


import com.langejansen.net.support.AllCells;
import com.langejansen.net.support.PulsePool;
import com.langejansen.net.support.errors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by tim on 15-4-17.
 */
public class CarrierCollection {


    /**
     * current queue and queue management for pulses
     */
    public PulsePool pulseQueue = new PulsePool.executor();
    private ConnectionList connectionList = new ConnectionList();
    private Logger logger = Logger.getLogger(getClass().getName());
    public CarrierCollection() {
    }

    /**
     * Shutdown all pulses and carriers
     */
    public void shutdown() {
        pulseQueue.shutdown();
    }

    /**
     * Emit a pulse to be sent to all connected cells
     * @param pulse pulse element
     */
    public void send(Pulse pulse) {
        logger.finer(pulse.emittingID + "\t sent a pulse of " + pulse.intensity);
        pulseQueue.add(pulse);
    }

    /**
     * Connect two cells.(oneway, default bias)
     *
     * @param from originating cell
     * @param to target cell
     */
    public void connect(cellClass from, cellClass to) {
        connectionList.connect(from,to);
    }

    /**
     * Connect two cells.(oneway, default bias)
     *
     * @param from originating cell
     * @param to target cell
     */
    public void connect(String from, String to) {
        connectionList.connect(from,to);
    }

    /**
     * Get all cells a cell is connected to
     * @param from originating cell
     * @return a list of all cells connected to
     */
    public List<cellClass> connectedTo(cellClass from) {
        return connectionList.connected(from);
    }

    /**
     * Get all cells a cell is connected from
     * @param to target cell
     * @return a list of all cells connected from
     */
    public List<cellClass> connectedFrom(cellClass to) {
        return connectionList.connectedfrom(to);
    }

    /**
     * Get all cells a cell is connected from
     * @param toID target cell ID
     * @return a list of all cells connected from
     */
    public List<cellClass> connectedFrom(String toID) {
        return connectedFrom(AllCells.getInstance().getCell(toID));
    }

    /**
     * Get all cells a cell is connected to
     * @param from originating cell
     * @return a list of all cell names connected to
     */
    public List<String> connectedToNames(String from) {
        return connectionList.connected(from);
    }

    /**
     * Get all cells a cell is connected from
     * @param to target cell
     * @return a list of all cells connected from
     */
    public List<String> connectedFromNames(String to) {
        return connectionList.connectedFrom(to);
    }

    public Boolean isConnectedTo(String fromCell, String toCell) {
        return connectionList.isConnectedTo(fromCell,toCell);
    }

    public Boolean isConnectedFrom(String fromCell, String toCell) {
        return connectionList.isConnectedFrom(fromCell,toCell);
    }

    /**
     * Get all cells a cell is connected to, by name
     * @param fromID originating cell ID
     * @return a list of all cells connected to
     */
    public List<cellClass> connectedTo(String fromID) {
        return connectedTo(AllCells.getInstance().getCell(fromID));
    }

    /**
     * disconnect a cell from everything
     * @param theCell the disconnecting cell
     */
    public void disconnect(cellClass theCell) {
        connectionList.disconnect(theCell);
    }

    /**
     * disconnect al cell connections from this cell to everything
     * @param theCell the disconnecting cell
     */
    public void disconnectFrom(cellClass theCell) {
        connectionList.disconnectFrom(theCell);
    }

    /**
     * disconnect al cell connections to this cell
     * @param theCell the disconnecting cell
     */
    public void disconnectTo(cellClass theCell) {
        connectionList.disconnectTo(theCell);
    }

    /**
     * remove all previous connections and build the connections from the registered bias'
     * @param theCell target cell
     */
    public void biasToConnections(cellClass theCell) {
        theCell.disconnect();

        HashMap<String, Double> biasList = theCell.getConnectionBias();
        AllCells allCells = AllCells.getInstance();

        //if there are bias' loaded from the json, then register the connections too
        for (String connectedCell: biasList.keySet()) {
            logger.finer("connected " + theCell.getName() + " to " + connectedCell);
            theCell.connectCell(allCells.getCell(connectedCell));
        }
    }

    public Double getBias(String fromName, String toName) {
        if (isConnectedTo(fromName, toName)) {
            AllCells allCells = AllCells.getInstance();
            return allCells.getCell(toName).getConnectionBias(fromName,1d);
        } else {
            return Double.NaN;
        }
    }


    public void setBias(String fromName, String toName, Double bias) throws errors.notConnectedError {
        if (isConnectedTo(fromName, toName)) {
            AllCells allCells = AllCells.getInstance();
            allCells.getCell(toName).setConnectionBias(fromName, bias);
        } else {
            throw new errors.notConnectedError(fromName, toName);
        }
    }

    public void clear() {
        connectionList.clear();
    }

    public class ConnectionList {
        HashMap<String, List<String>> connections = new HashMap<>();
        HashMap<String, List<String>> connections_reverse = new HashMap<>();

        private List<String> getOrCreate(String cellName, HashMap<String, List<String>> connectionMap) {
            List<String> connectionList = connectionMap.getOrDefault(cellName, new ArrayList<>());
            connectionMap.put(cellName, connectionList);
            return connectionList;
        }

        public void connect(cellClass from, cellClass to) {
            String fromName = from.getName();
            String toName = to.getName();
            connect(fromName, toName);
        }

        public void connect(String fromName, String toName) {
            if (fromName.equals(toName)) {
                return;
            }
            List<String> fromList = getOrCreate(fromName, connections);
            fromList.add(toName);
            List<String> toList = getOrCreate(toName, connections_reverse);
            if (!toList.contains(fromName)) {
                toList.add(fromName);
                logger.finer("connected " + fromName + " -> " + toName);
            }
        }

        public void disconnect(cellClass from, cellClass to) {
            String fromName = from.getName();
            String toName = to.getName();
            List<String> fromList = getOrCreate(fromName, connections);
            fromList.remove(toName);
            List<String> toList = getOrCreate(toName, connections_reverse);
            toList.remove(fromName);
        }

        private List<cellClass> namesToCells(List<String> names) {
            AllCells allCells = AllCells.getInstance();
            List<cellClass> retList = new ArrayList<>();
            for (String name : names) {
                retList.add(allCells.getCell(name));
            }
            return retList;
        }

        public List<cellClass> connected(cellClass from) {
            return namesToCells(getOrCreate(from.getName(), connections));
        }


        public List<String> connected(String from) {
            List<String> retList = new ArrayList<>();
            retList.addAll(getOrCreate(from, connections));
            return retList;
        }


        public List<String> connectedFrom(String to) {
            List<String> retList = new ArrayList<>();
            retList.addAll(getOrCreate(to, connections_reverse));
            return retList;
        }

        public List<cellClass> connectedfrom(cellClass to) {
            return namesToCells(getOrCreate(to.getName(), connections_reverse));
        }

        public void disconnect(cellClass theCell) {
            disconnect(theCell.getName());
        }

        public void disconnectFrom(cellClass theCell) {
            disconnectFrom(theCell.getName());
        }

        public void disconnectTo(cellClass theCell) {
            disconnectTo(theCell.getName());
        }

        public void disconnect(String cellName) {
            disconnectFrom(cellName);
            disconnectTo(cellName);
        }


        public void disconnectFrom(String cellName) {
            if (connections.containsKey(cellName)) {
                connections.remove(cellName);
            }
        }

        public void disconnectTo(String cellName) {
            if (connections_reverse.containsKey(cellName)) {
                for (String connectedTo : connections_reverse.get(cellName)) {
                    if (connections.containsKey(connectedTo)) {
                        List<String> c = connections.get(connectedTo);
                        if (c.contains(cellName)) {
                            c.remove(cellName);
                        }
                    }
                }
                connections_reverse.remove(cellName);
            }
        }

        public void clear() {
            connections.clear();
            connections_reverse.clear();
        }

        public Boolean isConnectedTo(String fromCell, String toCell) {
            return getOrCreate(fromCell, connections).contains(toCell);
        }

        public Boolean isConnectedFrom(String fromCell, String toCell) {
            return getOrCreate(fromCell, connections_reverse).contains(toCell);
        }
    }
}
