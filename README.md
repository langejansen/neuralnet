# README #

NeuralNet is a application of Artificial Neural Networks with various training methods.

### Contribution guidelines ###

* Every functionality should have a unit test accompanying them

### Who do I talk to? ###

* Want to join the effort? Contact tim@langejansen.com

### Maven dependencies ###

* com.google.code.gson:gson:2.8.02
* net.engio:mbassador:1.3.02