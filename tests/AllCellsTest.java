import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.langejansen.net.cellClass;
import com.langejansen.net.support.AllCells;
import com.langejansen.net.support.GeneralSettings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * Created by tim on 17-4-17.
 */
public class AllCellsTest {
    private Logger logger = null;

    public AllCellsTest() {
        setUpLogger();
    }

    private void setUpLogger() {
        logger = Logger.getLogger(getClass().getName());
        GeneralSettings.getInstance().generalInit();
    }

    @Before
    public void setUp() throws Exception {
        AllCells.getInstance().clear();
    }

    @After
    public void knockDown() throws Exception {

    }

    @org.junit.Test
    public void toJSON() throws Exception {
        AllCells.getInstance().clear();
        Logger logger = Logger.getLogger("com.langejansen");

        List<cellClass> cells = new ArrayList<>();

        cells.add(new cellClass.basic("c1"));
        for (Integer n = 2;n<=100;n++) {
            cellClass newCell = new cellClass.basic("c" + n);
            cells.get(cells.size()-1).connectCell(newCell);
            cells.add(newCell);
        }
        cellClass c1 = cells.get(0);
        cellClass cLast = cells.get(cells.size()-1);

        for (Integer n = 1;n<10;n++) {
            cells.get(1).connectCell(cells.get(((Double) (Math.random()*100)).intValue()));
        }
        JsonArray brainJson = AllCells.getInstance().toJSON();
        assertTrue("100 elements loaded",brainJson.size() == 100);

        JsonObject testCell = brainJson.get(1).getAsJsonObject();
        cellClass testingCell = AllCells.getInstance().getCell(testCell.get("cellName").getAsString());
        assertTrue("cell connected to as much others as the source data",testCell.get("connectionBias").getAsJsonArray().size() == testingCell.getConnectionBias().size());

    }

    private void assertTrue(String message, Boolean shouldbeTrue) {
        Logger logger = Logger.getLogger("com.langejansen");
        if (shouldbeTrue) {
            logger.info("Passed: " + message);
        } else {
            logger.severe("Failed: " + message);
            throw new Error("Failed: " + message);
        }
    }

    @org.junit.Test
    public void fromJson() throws Exception {
        AllCells allCells = AllCells.getInstance();
        allCells.clear();
        String JsonSource = "[{\"cellName\":\"c1\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c2\",\"value\":1.0}]},{\"cellName\":\"c2\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c3\",\"value\":1.0},{\"key\":\"c31\",\"value\":1.0},{\"key\":\"c77\",\"value\":1.0},{\"key\":\"c78\",\"value\":1.0},{\"key\":\"c18\",\"value\":1.0},{\"key\":\"c70\",\"value\":1.0},{\"key\":\"c73\",\"value\":1.0},{\"key\":\"c50\",\"value\":1.0},{\"key\":\"c83\",\"value\":1.0}]},{\"cellName\":\"c3\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c4\",\"value\":1.0}]},{\"cellName\":\"c4\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c5\",\"value\":1.0}]},{\"cellName\":\"c5\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c6\",\"value\":1.0}]},{\"cellName\":\"c6\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c7\",\"value\":1.0}]},{\"cellName\":\"c7\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c8\",\"value\":1.0}]},{\"cellName\":\"c8\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c9\",\"value\":1.0}]},{\"cellName\":\"c9\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c10\",\"value\":1.0}]},{\"cellName\":\"c10\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c11\",\"value\":1.0}]},{\"cellName\":\"c11\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c12\",\"value\":1.0}]},{\"cellName\":\"c12\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c13\",\"value\":1.0}]},{\"cellName\":\"c13\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c14\",\"value\":1.0}]},{\"cellName\":\"c14\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c15\",\"value\":1.0}]},{\"cellName\":\"c15\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c16\",\"value\":1.0}]},{\"cellName\":\"c16\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c17\",\"value\":1.0}]},{\"cellName\":\"c17\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c18\",\"value\":1.0}]},{\"cellName\":\"c18\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c19\",\"value\":1.0}]},{\"cellName\":\"c19\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c20\",\"value\":1.0}]},{\"cellName\":\"c20\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c21\",\"value\":1.0}]},{\"cellName\":\"c21\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c22\",\"value\":1.0}]},{\"cellName\":\"c22\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c23\",\"value\":1.0}]},{\"cellName\":\"c23\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c24\",\"value\":1.0}]},{\"cellName\":\"c24\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c25\",\"value\":1.0}]},{\"cellName\":\"c25\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c26\",\"value\":1.0}]},{\"cellName\":\"c26\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c27\",\"value\":1.0}]},{\"cellName\":\"c27\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c28\",\"value\":1.0}]},{\"cellName\":\"c28\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c29\",\"value\":1.0}]},{\"cellName\":\"c29\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c30\",\"value\":1.0}]},{\"cellName\":\"c30\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c31\",\"value\":1.0}]},{\"cellName\":\"c31\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c32\",\"value\":1.0}]},{\"cellName\":\"c32\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c33\",\"value\":1.0}]},{\"cellName\":\"c33\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c34\",\"value\":1.0}]},{\"cellName\":\"c34\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c35\",\"value\":1.0}]},{\"cellName\":\"c35\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c36\",\"value\":1.0}]},{\"cellName\":\"c36\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c37\",\"value\":1.0}]},{\"cellName\":\"c37\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c38\",\"value\":1.0}]},{\"cellName\":\"c38\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c39\",\"value\":1.0}]},{\"cellName\":\"c39\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c40\",\"value\":1.0}]},{\"cellName\":\"c40\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c41\",\"value\":1.0}]},{\"cellName\":\"c41\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c42\",\"value\":1.0}]},{\"cellName\":\"c42\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c43\",\"value\":1.0}]},{\"cellName\":\"c43\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c44\",\"value\":1.0}]},{\"cellName\":\"c44\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c45\",\"value\":1.0}]},{\"cellName\":\"c45\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c46\",\"value\":1.0}]},{\"cellName\":\"c46\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c47\",\"value\":1.0}]},{\"cellName\":\"c47\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c48\",\"value\":1.0}]},{\"cellName\":\"c48\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c49\",\"value\":1.0}]},{\"cellName\":\"c49\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c50\",\"value\":1.0}]},{\"cellName\":\"c50\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c51\",\"value\":1.0}]},{\"cellName\":\"c51\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c52\",\"value\":1.0}]},{\"cellName\":\"c52\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c53\",\"value\":1.0}]},{\"cellName\":\"c53\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c54\",\"value\":1.0}]},{\"cellName\":\"c54\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c55\",\"value\":1.0}]},{\"cellName\":\"c55\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c56\",\"value\":1.0}]},{\"cellName\":\"c56\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c57\",\"value\":1.0}]},{\"cellName\":\"c57\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c58\",\"value\":1.0}]},{\"cellName\":\"c58\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c59\",\"value\":1.0}]},{\"cellName\":\"c59\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c60\",\"value\":1.0}]},{\"cellName\":\"c60\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c61\",\"value\":1.0}]},{\"cellName\":\"c61\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c62\",\"value\":1.0}]},{\"cellName\":\"c62\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c63\",\"value\":1.0}]},{\"cellName\":\"c63\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c64\",\"value\":1.0}]},{\"cellName\":\"c64\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c65\",\"value\":1.0}]},{\"cellName\":\"c65\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c66\",\"value\":1.0}]},{\"cellName\":\"c66\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c67\",\"value\":1.0}]},{\"cellName\":\"c67\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c68\",\"value\":1.0}]},{\"cellName\":\"c68\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c69\",\"value\":1.0}]},{\"cellName\":\"c69\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c70\",\"value\":1.0}]},{\"cellName\":\"c70\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c71\",\"value\":1.0}]},{\"cellName\":\"c71\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c72\",\"value\":1.0}]},{\"cellName\":\"c72\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c73\",\"value\":1.0}]},{\"cellName\":\"c73\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c74\",\"value\":1.0}]},{\"cellName\":\"c74\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c75\",\"value\":1.0}]},{\"cellName\":\"c75\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c76\",\"value\":1.0}]},{\"cellName\":\"c76\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c77\",\"value\":1.0}]},{\"cellName\":\"c77\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c78\",\"value\":1.0}]},{\"cellName\":\"c78\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c79\",\"value\":1.0}]},{\"cellName\":\"c79\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c80\",\"value\":1.0}]},{\"cellName\":\"c80\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c81\",\"value\":1.0}]},{\"cellName\":\"c81\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c82\",\"value\":1.0}]},{\"cellName\":\"c82\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c83\",\"value\":1.0}]},{\"cellName\":\"c83\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c84\",\"value\":1.0}]},{\"cellName\":\"c84\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c85\",\"value\":1.0}]},{\"cellName\":\"c85\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c86\",\"value\":1.0}]},{\"cellName\":\"c86\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c87\",\"value\":1.0}]},{\"cellName\":\"c87\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c88\",\"value\":1.0}]},{\"cellName\":\"c88\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c89\",\"value\":1.0}]},{\"cellName\":\"c89\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c90\",\"value\":1.0}]},{\"cellName\":\"c90\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c91\",\"value\":1.0}]},{\"cellName\":\"c91\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c92\",\"value\":1.0}]},{\"cellName\":\"c92\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c93\",\"value\":1.0}]},{\"cellName\":\"c93\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c94\",\"value\":1.0}]},{\"cellName\":\"c94\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c95\",\"value\":1.0}]},{\"cellName\":\"c95\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c96\",\"value\":1.0}]},{\"cellName\":\"c96\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c97\",\"value\":1.0}]},{\"cellName\":\"c97\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c98\",\"value\":1.0}]},{\"cellName\":\"c98\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c99\",\"value\":1.0}]},{\"cellName\":\"c99\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c100\",\"value\":1.0}]},{\"cellName\":\"c100\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[]}]";
        allCells.fromJson(JsonSource);

        assertTrue("Json contains 100 cells, 100 cells loaded", allCells.getAll().size() == 100);
        assertTrue("Cell c2 in json has 9 connections, loaded has same", allCells.getCell("c2").getConnectionBias().size() == 9);
    }

    @org.junit.Test
    public void sendPulseChain() throws Exception {
        Logger logger = Logger.getLogger("com.langejansen");
        AllCells.getInstance().clear();

        List<cellClass> cells = new ArrayList<>();

        cells.add(new cellClass.basic("c1"));
        for (Integer n = 2;n<=10000;n++) {
            cellClass newCell = new cellClass.basic("c" + n);
            cells.get(cells.size()-1).connectCell(newCell);
            cells.add(newCell);
        }
        cellClass c1 = cells.get(0);
        cellClass cLast = cells.get(cells.size()-1);
        cLast.setTriggerLevel(10000d);

        c1.sendPulse(1d);
        assertTrue("pulse propagated to the last cell in the chain", cLast.awaitPulse(15));
        AllCells.getInstance().carriers().pulseQueue.runfor(5l);
        c1.setTriggerLevel(10000d);
        cLast.sendPulse(1d);
        assertFalse("pulse should not propagate the wrong way", c1.awaitPulse(15));
    }

    private void assertFalse(String message, Boolean shouldbeFalse) {
        assertTrue(message, !shouldbeFalse);
    }

    @org.junit.Test
    public void sendPulseChainBenchMark() throws Exception {
        Logger logger = Logger.getLogger("com.langejansen");

        int numCells = 100000;
        Integer targetTimeMs = 1000;
        Integer runtime = 0;
        logger.info("num cells set to " + numCells);

        int totals = 0;
        logger.info("clearing");
        AllCells.getInstance().clear();
        List<cellClass> cells = new ArrayList<>();
        logger.info("setting up");

        cells.add(new cellClass.basic("c1"));
        while (cells.size() < numCells) {
            Integer n = cells.size()+1;
            cellClass newCell = new cellClass.basic("c" + n);
            cells.get(cells.size() - 1).connectCell(newCell);
            cells.add(newCell);
        }
        cellClass c1 = cells.get(0);
        cellClass cLast = cells.get(cells.size() - 1);
        cLast.setTriggerLevel(10000d);
        c1.setPotential(0d);
        logger.info("/setting up");

        for (int n = 0;n<10;n++) {
            runtime = null;
            while (runtime == null) {
                c1.sendPulse(1d);
                Long returnedTime = cLast.timePulseMs(15);
                if (returnedTime != null) {
                    runtime = returnedTime.intValue();
                }
            }
            totals += runtime;
        }
        runtime = totals/10;
        Double pulsesAsec = 1000d/(runtime.doubleValue()/((Integer) numCells).doubleValue());
        logger.info("Average over 10 runs: " + runtime + "ms");
        DecimalFormat df = new DecimalFormat("### ### ###.00");
        logger.info(df.format(pulsesAsec) + " pulses/sec");


    }

    @org.junit.Test
    public void sendPulseNet() throws Exception {
        AllCells allCells = AllCells.getInstance();

        List<cellClass> cells = new ArrayList<>();

        int numLayers = 3;
        int numWide = 3;

        cells.add(new cellClass.basic("first"));
        for (Integer x1=1;x1<=numWide;x1++) {
            cells.add(new cellClass.basic("l1c" + x1));
            allCells.carriers().connect("first","l1c" + x1);
        }

        Integer n;
        for (n=1;n<=numLayers;n++) {
            for (Integer x1=1;x1<=numWide;x1++) {
                for (Integer x2=1;x2<=numWide;x2++) {
                    String cellFrom ="l" + n + "c" + x1;
                    String cellTo =  "l" + (n+1) +"c" + x2;
                    cells.add(new cellClass.basic(cellTo));
                    allCells.carriers().connect(cellFrom, cellTo);
                }
            }
        }

        for (Integer x2=1;x2<=numWide;x2++) {
            String cellFrom =  "l" + n +"c" + x2;
            allCells.getCell(cellFrom).setTriggerLevel(10000d);
        }

        cellClass c1 = cells.get(0);
        cellClass cLast = cells.get(cells.size()-1);
        cLast.setTriggerLevel(10000d);

        c1.sendPulse(1d);
        Long waitedFor = cLast.timePulseMs(15);
        if (waitedFor == null) {
            waitedFor = 0l;
        }
        logger.info("Waited for " + waitedFor + "ms");

        assertTrue("pulse propagated to the last cell", cLast.getPotential() > 0d);

        for (Integer x2=1;x2<=numWide;x2++) {
            String cellFrom =  "l" + n +"c" + x2;
            assertTrue("pulse propagated to the last cells in the chain: 1 = "+ cLast.getPotential(), cLast.getPotential() >0d);
        }
    }

    @Test
    public void heartbeatBeats() throws Exception {
        //a heartcell should beat
        AllCells allCells = AllCells.getInstance();

        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        //does it beat?

        //make a heartbeat, connect it to a new cell
        logger.info("make a heartbeat, connect it to a new cell");
        cellClass.heartbeat heart = new cellClass.heartbeat("heart");
        cellClass.basic cell = new cellClass.basic("c1");
        cell.setTriggerLevel(10000d);
        heart.setTriggerLevel(0.5);
        heart.connectCell(cell);
        //start 'em beating
        allCells.startBeats();
        //wait for a bit and see if the cell got some pulses
        logger.info("wait for a bit");
        Thread.sleep(5000);
        assertTrue("Heart beat several times: " + cell.getPotential(),cell.getPotential() >= 10);
    }

    @Test
    public void heartbeatVary() throws Exception {
        //a heart cell should beat
        //a heart cell frequency should vary with the triggerlevel
        //a heart cell frequency should vary by a received pulse intensity
        //a heart cell should start beating once it recieved a pulse


        AllCells allCells = AllCells.getInstance();
        allCells.clear();

        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        //make a heartbeat, connect it to a new cell
        logger.info("make a heartbeat, connect it to a new cell");
        cellClass cellSource = new cellClass.basic("source");
        cellClass.heartbeat heart = new cellClass.heartbeat("heart");
        cellSource.connectCell(heart);
        cellClass.basic cell = new cellClass.basic("c1");
        cell.setTriggerLevel(10000d);
        heart.setTriggerLevel(1d);
        heart.connectCell(cell);

        //send a pulse to get it started
        cellSource.sendPulse(1d);
        Thread.sleep(10100);
        allCells.stopBeats();
        Integer collectedPulses = cell.getPotential().intValue();
        assertTrue("a 1.0 pulse results in a 1.0 triggerlevel/1.0 pulse",heart.getTriggerLevel() == 1d);
        assertTrue("Heart beat ~10 times at 1.0 triggerlevel/1.0 pulse: " + collectedPulses, collectedPulses >= 9d && collectedPulses <= 12d);

        //do the same with half the impulse strength
        cell.setPotential(0d);
        cellSource.sendPulse(2d);
        Thread.sleep(10100);
        allCells.stopBeats();
        Integer collectedPulsesHalf = cell.getPotential().intValue();
        assertTrue("a 2.0 pulse results in a 0.5 triggerlevel/2.0 pulse",heart.getTriggerLevel() == 0.5d);
        assertTrue("Heart beat ~20 times at 0.5 triggerlevel/2.0 pulse: " + collectedPulsesHalf, collectedPulsesHalf >= 19d && collectedPulsesHalf <= 21d);

    }

    @Test
    public void heartbeatToJson() throws Exception {
        AllCells allCells = AllCells.getInstance();
        allCells.clear();

        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        //make a heartbeat, connect it to a new cell
        logger.info("make a heartbeat, connect it to a new cell");
        cellClass cellSource = new cellClass.basic("source");
        cellClass.heartbeat heart = new cellClass.heartbeat("heart");
        cellSource.connectCell(heart);
        cellClass.basic cell = new cellClass.basic("c1");
        cell.setTriggerLevel(10000d);
        heart.setTriggerLevel(1d);
        heart.connectCell(cell);

        JsonObject heartJson = heart.toJSON();
        for (String propertyName: new String[] {"cellName","potential","triggerLevel","connectionBias","type"}) {
            assertTrue("Has value " + propertyName,heartJson.has(propertyName));
        }
        assertTrue("convert to Json: Name converted correctly ", heartJson.get("cellName").getAsString().equals("heart"));
        assertTrue("convert to Json: potential converted correctly ", heartJson.get("potential").getAsDouble() == 0d);
        assertTrue("convert to Json: triggerLevel converted correctly ", heartJson.get("triggerLevel").getAsDouble() == 1.0d);
        assertTrue("convert to Json: type converted correctly ", heartJson.get("type").getAsString().equals("heartbeat"));
        assertTrue("convert to Json: connection converted correct", heartJson.getAsJsonArray("connectionBias").getAsJsonArray().get(0).getAsJsonObject().get("key").getAsString() == "c1" && heartJson.getAsJsonArray("connectionBias").getAsJsonArray().get(0).getAsJsonObject().get("value").getAsDouble() == 1d);
        assertTrue("convert to Json: connection number converted correct", heartJson.getAsJsonArray("connectionBias").getAsJsonArray().size() == 1);
    }

    @Test
    public void heartbeatJsonCanLoadExport() throws Exception {
        AllCells allCells = AllCells.getInstance();
        allCells.clear();

        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        //make a heartbeat, connect it to a new cell
        logger.info("make a heartbeat, connect it to a new cell");
        cellClass cellSource = new cellClass.basic("source");
        cellClass.heartbeat heart = new cellClass.heartbeat("heart");
        cellSource.connectCell(heart);
        cellClass.basic cell = new cellClass.basic("c1");
        cell.setTriggerLevel(10000d);
        heart.setTriggerLevel(1d);
        heart.connectCell(cell);

        JsonObject heartJson = heart.toJSON();

        cellClass.heartbeat loadedHeart = new cellClass.heartbeat(heartJson);
        assertTrue("convert from Json: Name read correctly ", loadedHeart.getName().equals("heart"));
        assertTrue("convert from Json: potential read correctly ", loadedHeart.getPotential() == 0d);
        assertTrue("convert from Json: triggerLevel read correctly ", loadedHeart.getTriggerLevel() == 1.0d);
        assertTrue("convert from Json: type read correctly ", loadedHeart.getClass().getSimpleName().equals("heartbeat"));
        assertTrue("convert from Json: connection loaded correct: target", loadedHeart.getConnectionBias().containsKey("c1"));
        assertTrue("convert from Json: connection loaded correct: bias", loadedHeart.getConnectionBias().get("c1") == 1.0);
        assertTrue("convert from Json: connection number loaded correct", loadedHeart.getConnectionBias().size() == 1);
    }

    @Test
    public void heartFromJson() throws Exception {
        cellClass.heartbeat loadedHeart;
        AllCells allCells = AllCells.getInstance();

        allCells.clear();
        String JsonSource = "[{\"cellName\":\"source\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"heart\",\"value\":1.0}],\"type\":\"basic\"},{\"cellName\":\"heart\",\"potential\":0.0,\"triggerLevel\":1.0,\"connectionBias\":[{\"key\":\"c1\",\"value\":1.0}],\"type\":\"heartbeat\"},{\"cellName\":\"c1\",\"potential\":0.0,\"triggerLevel\":10000.0,\"connectionBias\":[],\"type\":\"basic\"}]";
        allCells.fromJson(JsonSource);
        
        loadedHeart = (cellClass.heartbeat) allCells.getCell("heart");
        assertTrue("load heart cell from Json: Name read correctly ", loadedHeart.getName().equals("heart"));
        assertTrue("load heart cell from Json: potential read correctly ", loadedHeart.getPotential() == 0d);
        assertTrue("load heart cell from Json: triggerLevel read correctly ", loadedHeart.getTriggerLevel() == 1.0d);
        assertTrue("load heart cell from Json: type read correctly ", loadedHeart.getClass().getSimpleName().equals("heartbeat"));
        assertTrue("load heart cell from Json: connection loaded correct: target", loadedHeart.getConnectionBias().containsKey("c1"));
        assertTrue("load heart cell from Json: connection loaded correct: bias", loadedHeart.getConnectionBias().get("c1") == 1.0);
        assertTrue("load heart cell from Json: connection number loaded correct", loadedHeart.getConnectionBias().size() == 1);
    }

}