import com.langejansen.net.CarrierCollection;
import com.langejansen.net.Pulse;
import com.langejansen.net.cellClass;
import com.langejansen.net.support.AllCells;
import com.langejansen.net.support.AllStressors;
import com.langejansen.net.support.GeneralSettings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

/**
 * Created by tim on 21-4-17.
 */
public class stressorTest {
    private Logger logger = null;

    public stressorTest() {
        setUpLogger();
    }

    private void setUpLogger() {
        logger = Logger.getLogger(getClass().getName());
        GeneralSettings.getInstance().generalInit();
    }

    private void assertFalse(String message, Boolean shouldbeFalse) {
        assertTrue(message, !shouldbeFalse);
    }

    private void assertTrue(String message,Boolean shouldbeTrue) {
        Logger logger = Logger.getLogger("com.langejansen");
        if (shouldbeTrue) {
            logger.info("Passed: " + message);
        } else {
            logger.severe("Failed: " + message);
            throw new Error("Failed: " + message);
        }
    }

    @Before
    public void setUp() throws Exception {
        AllCells.getInstance().clear();
    }

    @After
    public void tearDown() throws Exception {

    }

    /**
     * Tests:
     * - associate stressor with cell
     * - stressor adopts connections and bias' of associated cells
     * - stress pulse propagates forward
     * - stress pulse does not propagate backwards
     * - stress pulse influences conneciton bias'
     *
     * @throws Exception
     */
    @Test
    public void testcollectAndAdoptBiasForward() throws Exception {
        AllCells allCells = AllCells.getInstance();
        CarrierCollection allCarriers = allCells.carriers();
        AllStressors allStressors = AllStressors.getInstance();

        cellClass c1 = new cellClass.basic("c1");
        cellClass c2 = new cellClass.basic("c2");
        cellClass c3 = new cellClass.basic("c3");
        cellClass c4 = new cellClass.basic("c4");

        cellClass.stressor s1 = new cellClass.stressor(c1);
        cellClass.stressor s2 = new cellClass.stressor(c2);
        cellClass.stressor s3 = new cellClass.stressor(c3);
        cellClass.stressor s4 = new cellClass.stressor(c4);

        s1.setBackPropagating(false);
        s2.setBackPropagating(false);
        s3.setBackPropagating(false);
        s4.setBackPropagating(false);

        c1.connectCell(c2);
        c1.connectCell(c3);
        c1.connectCell(c4);
        assertTrue("check setup", allCarriers.isConnectedTo("c1", "c2") && allCarriers.isConnectedTo("c1", "c3") && allCarriers.isConnectedTo("c1", "c4"));

        allStressors.adoptAssociated();

        s2.getCell().sendPulse(2d);
        s3.getCell().sendPulse(2d);
        s4.getCell().sendPulse(2d);
        allCarriers.pulseQueue.runfor(2l);
        assertTrue("check effect of pulse c2-c1", allCarriers.getBias("c1", "c2").equals(1d));
        assertTrue("check effect of pulse c3-c1", allCarriers.getBias("c1", "c3").equals(1d));
        assertTrue("check effect of pulse c4-c1", allCarriers.getBias("c1", "c4").equals(1d));

        s1.getCell().sendPulse(2d);
        allCarriers.pulseQueue.runfor(5l);
        assertTrue("check effect of pulse c1-c2", allCarriers.getBias("c1", "c2").equals(0.5d));
        assertTrue("check effect of pulse c1-c3", allCarriers.getBias("c1", "c3").equals(0.5d));
        assertTrue("check effect of pulse c1-c4", allCarriers.getBias("c1", "c4").equals(0.5d));

        assertTrue("check result connections forwardpropagating c2-c1 ", allCarriers.isConnectedFrom("$-c2", "$-c1"));
        assertTrue("check result connections forwardpropagating c3-c1 ", allCarriers.isConnectedFrom("$-c3", "$-c1"));
        assertTrue("check result connections forwardpropagating c4-c1 ", allCarriers.isConnectedFrom("$-c4", "$-c1"));
    }

    /**
     * Tests:
     * - associate stressor with cell
     * - stressor adopts connections and bias' of associated cells
     * - backward propagating stress pulse propagates backward
     * - backward propagating stress pulse does not propagate forwards
     * - stress pulse influences conneciton bias'
     * @throws Exception
     */
    @Test
    public void testcollectAndAdoptBiasBackward() throws Exception {
        AllCells allCells = AllCells.getInstance();
        CarrierCollection allCarriers = allCells.carriers();
        AllStressors allStressors = AllStressors.getInstance();

        cellClass c1 = new cellClass.basic("c1");
        cellClass c2 = new cellClass.basic("c2");
        cellClass c3 = new cellClass.basic("c3");
        cellClass c4 = new cellClass.basic("c4");

        cellClass.stressor s1 = new cellClass.stressor(c1);
        cellClass.stressor s2 = new cellClass.stressor(c2);
        cellClass.stressor s3 = new cellClass.stressor(c3);
        cellClass.stressor s4 = new cellClass.stressor(c4);

        s1.setBackPropagating(true);
        s2.setBackPropagating(true);
        s3.setBackPropagating(true);
        s4.setBackPropagating(true);

        c1.connectCell(c2);
        c1.connectCell(c3);
        c1.connectCell(c4);
        assertTrue("check setup", allCarriers.isConnectedTo("c1", "c2") && allCarriers.isConnectedTo("c1", "c3") && allCarriers.isConnectedTo("c1", "c4"));

        allStressors.adoptAssociated();

        s1.getCell().sendPulse(2d);
        allCarriers.pulseQueue.runfor(2l);
        assertTrue("check effect of pulse c2-c1", allCarriers.getBias("c1", "c2").equals(1d));
        assertTrue("check effect of pulse c3-c1", allCarriers.getBias("c1", "c3").equals(1d));
        assertTrue("check effect of pulse c4-c1", allCarriers.getBias("c1", "c4").equals(1d));

        s2.getCell().sendPulse(2d);
        allCarriers.pulseQueue.runfor(2l);
        assertTrue("check effect of pulse c2-c1 @c2", allCarriers.getBias("c1", "c2").equals(0.5d));
        assertTrue("check effect of pulse c2-c1 @c3", allCarriers.getBias("c1", "c3").equals(1d));
        assertTrue("check effect of pulse c2-c1 @c4", allCarriers.getBias("c1", "c4").equals(1d));
        s3.getCell().sendPulse(2d);
        allCarriers.pulseQueue.runfor(2l);
        assertTrue("check effect of pulse c3-c1 @c2", allCarriers.getBias("c1", "c2").equals(0.5d));
        assertTrue("check effect of pulse c3-c1 @c3", allCarriers.getBias("c1", "c3").equals(0.5d));
        assertTrue("check effect of pulse c3-c1 @c4", allCarriers.getBias("c1", "c4").equals(1d));
        s4.getCell().sendPulse(2d);
        allCarriers.pulseQueue.runfor(2l);
        assertTrue("check effect of pulse c4-c1 @c2", allCarriers.getBias("c1", "c2").equals(0.5d));
        assertTrue("check effect of pulse c4-c1 @c3", allCarriers.getBias("c1", "c3").equals(0.5d));
        assertTrue("check effect of pulse c4-c1 @c4", allCarriers.getBias("c1", "c4").equals(0.5d));


        assertTrue("check result connections backward propagating c2-c1 ", allCarriers.isConnectedFrom("$-c2", "$-c1"));
        assertTrue("check result connections backward propagating c3-c1 ", allCarriers.isConnectedFrom("$-c3", "$-c1"));
        assertTrue("check result connections backward propagating c4-c1 ", allCarriers.isConnectedFrom("$-c4", "$-c1"));

        //check pulse
        Pulse pulsefw = new Pulse.pullmodel("$-c1", 1d);
        List<cellClass> recipientsfw = pulsefw.getRecipients();
        int pulseSizefw = recipientsfw.size();

    }

    /**
     * stressor can influence the triggerlevel of the associated cell
     * @throws Exception
     */
    @Test
    public void influenceTriggerLevel() throws Exception {
        List<cellClass.basic> cells = new ArrayList<>();
        List<cellClass.stressor> stressors = new ArrayList<>();
        AllCells allCells = AllCells.getInstance();
        cells.add(new cellClass.basic("c" + 0));
        cellClass firstCell =cells.get(0);
        stressors.add(new cellClass.stressor(firstCell));
        cellClass.stressor firstStressor = stressors.get(0);
        firstStressor.setAdoptBias(true);
        firstStressor.cellStressor = true;
        for (int n=1;n<10;n++) {
            cellClass.basic influencedCell = new cellClass.basic("c" + n);
            allCells.carriers().connect(cells.get(n-1),influencedCell);

            cellClass.stressor stressCell = new cellClass.stressor(influencedCell);
            stressCell.setAdoptBias(true);
            stressCell.cellStressor = true;

            cells.add(influencedCell);
            stressors.add(stressCell);
        }
        AllStressors.getInstance().adoptAssociated();
        cellClass lastCell =cells.get(cells.size()-1);
        cellClass secondCell =cells.get(1);

        cellClass.stressor lastStressor = stressors.get(stressors.size()-1);


        HashMap<Double,Double> io = new HashMap<>();


        lastStressor.getCell().sendPulse(2d);
        allCells.carriers().pulseQueue.runfor(5l);
        System.out.println("---------------------------------------");
        assertEquals("triggerlevel of first cell in chain was affected by the stress", 0.5d,firstCell.getTriggerLevel(),0d);
        logger.info("Works!");
    }

    @Test
    public void testConnectedTest() throws Exception {
        assertTrue("make this!", Boolean.TRUE);
    }

    @Test
    public void testToString() throws Exception {
        List<cellClass.basic> cells = new ArrayList<>();
        List<cellClass.stressor> stressors = new ArrayList<>();
        AllCells allCells = AllCells.getInstance();

        cells.add(new cellClass.basic("c" + 0));
        cellClass firstCell = cells.get(0);
        stressors.add(new cellClass.stressor(firstCell));
        cellClass.stressor firstStressor = stressors.get(0);
        assertTrue("output is correct", firstStressor.toString().equals("{\"associatedCell\":\"c0\",\"stressCell\":\"$-c0\"}"));
    }

    @Test
    public void getAssociated() throws Exception {

    }

}