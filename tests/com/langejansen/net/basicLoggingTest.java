package com.langejansen.net;

import com.langejansen.net.support.GeneralSettings;
import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by langejansen on 19-6-17.
 */
public class basicLoggingTest {
    Logger logger = Logger.getLogger("com.langejansen");

    @Test
    public void sendPulse() throws Exception {
        setUpLogger();

        logger.setLevel(Level.FINEST);

        cellClass cell1 = new cellClass.basic("cell1");
        cellClass.basicLogging loggingCell = new cellClass.basicLogging("loggingcell");
        cell1.connectCell(loggingCell);
        cell1.sendPulse(10d);
        loggingCell.awaitPulse(10);
    }

    private void setUpLogger() {
        logger = Logger.getLogger(getClass().getName());
        GeneralSettings.getInstance().generalInit();
    }

}