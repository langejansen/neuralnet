import com.google.gson.JsonObject;
import com.langejansen.trainer.TrainingData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.logging.*;

/**
 * Created by tim on 19-4-17.
 */
public class TrainingDataTest {
    private Logger logger = Logger.getLogger("com.langejansen");

    private void assertFalse(String message, Boolean shouldbeFalse) {
        assertTrue(message, !shouldbeFalse);
    }

    private void assertTrue(String message,Boolean shouldbeTrue) {
        Logger logger = Logger.getLogger("com.langejansen");
        if (shouldbeTrue) {
            logger.info("Passed: " + message);
        } else {
            logger.severe("Failed: " + message);
            throw new Error("Failed: " + message);
        }
    }

    @Test
    public void testSimpleSetGetInput() throws Exception {
        TrainingData.simple trainingData = new TrainingData.simple();
        Double[] inputSet = new Double[] {1d,2d,3d,4d,5d};
        trainingData.setInputs(inputSet);
        List<Double> tdInput = trainingData.inputs();
        for (Integer n = 0; n<tdInput.size();n++) {
            assertTrue("Input " + n + " is " + inputSet[n], Objects.equals(tdInput.get(n), inputSet[n]));
        }
    }

    @Test
    public void testSimpleToJson() throws Exception {
        TrainingData.simple trainingData = new TrainingData.simple();
        HashMap<Integer, Double> tdOutPuts = trainingData.outputs();
        tdOutPuts.put(1, 1d);
        tdOutPuts.put(2, 5d);
        tdOutPuts.put(3, 6d);
        tdOutPuts.put(4, 7d);
        tdOutPuts.put(5, 9d);
        tdOutPuts.put(6, 2382d);
        Double[] inputSet = new Double[] {1d,2d,3d,4d,5d};
        trainingData.setInputs(inputSet);

        JsonObject j = trainingData.toJson();
        String jsonString = trainingData.toString();
        TrainingData.simple jsonLoaded = new TrainingData.simple(j);
        TrainingData.simple strLoaded = new TrainingData.simple(jsonString);

        assertTrue("json exported and then loaded is equal to the original", trainingData.equals(jsonLoaded));
        assertTrue("json exported, converted to string and then loaded is equal to the original", trainingData.equals(strLoaded));

    }

    @Test
    public void testSimpleFromString() throws Exception {
        TrainingData.simple trainingData = new TrainingData.simple();
        HashMap<Integer, Double> tdOutPuts = trainingData.outputs();
        tdOutPuts.put(1, 1d);
        tdOutPuts.put(2, 5d);
        tdOutPuts.put(3, 6d);
        tdOutPuts.put(4, 7d);
        tdOutPuts.put(5, 9d);
        tdOutPuts.put(6, 2382d);
        Double[] inputSet = new Double[] {1d,2d,3d,4d,5d};
        trainingData.setInputs(inputSet);

        String jsonString = "{\"inputs\":[1.0,2.0,3.0,4.0,5.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}";
        TrainingData.simple strLoaded = new TrainingData.simple(jsonString);

        assertTrue("json string loaded is equal to the original", trainingData.equals(strLoaded));
    }

    @Test
    public void testDeltaT() throws Exception {
        //make a deltaT dataset
        TrainingData.deltaT deltaT = new TrainingData.deltaT(100d);

        List<TrainingData.simple> testdataList = new ArrayList<>();
        //make a few datapoints
        for (Integer n=0;n<10;n++) {
            TrainingData.simple trainingData = new TrainingData.simple();
            HashMap<Integer, Double> tdOutPuts = trainingData.outputs();
            tdOutPuts.put(1, 1d);
            tdOutPuts.put(2, 5d);
            tdOutPuts.put(3, 6d);
            tdOutPuts.put(4, 7d);
            tdOutPuts.put(5, 9d);
            tdOutPuts.put(6, 2382d);
            Double[] inputSet = new Double[]{
                    1d + n.doubleValue(),
                    2d + n.doubleValue(),
                    3d + n.doubleValue(),
                    4d + n.doubleValue(),
                    5d + n.doubleValue()};
            trainingData.setInputs(inputSet);
            testdataList.add(trainingData);
        }
        //add them to the deltaT
        for (TrainingData.simple trainingData: testdataList) {
            deltaT.add(trainingData);
        }

        //Check if the order and contents are still the same
        for (Integer n=0;n<testdataList.size();n++) {
            assertTrue("#" + n + " was added correctly", deltaT.get(n).equals(testdataList.get(n)));
        }

    }

    @Test
    public void testDeltaTjson() throws Exception {
        //make a deltaT dataset
        TrainingData.deltaT deltaT = new TrainingData.deltaT(100d);

        List<TrainingData.simple> testdataList = new ArrayList<>();
        //make a few datapoints
        for (Integer n=0;n<10;n++) {
            TrainingData.simple trainingData = new TrainingData.simple();
            HashMap<Integer, Double> tdOutPuts = trainingData.outputs();
            tdOutPuts.put(1, 1d);
            tdOutPuts.put(2, 5d);
            tdOutPuts.put(3, 6d);
            tdOutPuts.put(4, 7d);
            tdOutPuts.put(5, 9d);
            tdOutPuts.put(6, 2382d);
            Double[] inputSet = new Double[]{
                    1d + n.doubleValue(),
                    2d + n.doubleValue(),
                    3d + n.doubleValue(),
                    4d + n.doubleValue(),
                    5d + n.doubleValue()};
            trainingData.setInputs(inputSet);
            testdataList.add(trainingData);
        }
        //add them to the deltaT
        for (TrainingData.simple trainingData: testdataList) {
            deltaT.add(trainingData);
        }
        //export to json
        JsonObject j = deltaT.toJson();
        //export to string
        String jsonString = deltaT.toString();
        //import from string
        TrainingData.deltaT deltaTfromString = new TrainingData.deltaT(jsonString);
        //Check if the order and contents are the same as the original
        for (Integer n=0;n<testdataList.size();n++) {
            assertTrue("#" + n + " was added correctly", deltaTfromString.get(n).equals(testdataList.get(n)));
        }
        assertTrue("interval was read correctly",deltaTfromString.getMsInterval().equals(deltaT.getMsInterval()));

    }

    @Test
    public void testdeltaTfromJson() throws Exception {
        String jsonString = "{\"frames\":[{\"inputs\":[1.0,2.0,3.0,4.0,5.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}, {\"inputs\":[2.0,3.0,4.0,5.0,6.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}, {\"inputs\":[3.0,4.0,5.0,6.0,7.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}, {\"inputs\":[4.0,5.0,6.0,7.0,8.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}, {\"inputs\":[5.0,6.0,7.0,8.0,9.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}, {\"inputs\":[6.0,7.0,8.0,9.0,10.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}, {\"inputs\":[7.0,8.0,9.0,10.0,11.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}, {\"inputs\":[8.0,9.0,10.0,11.0,12.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}, {\"inputs\":[9.0,10.0,11.0,12.0,13.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}, {\"inputs\":[10.0,11.0,12.0,13.0,14.0],\"outputs\":{\"1\":1.0,\"2\":5.0,\"3\":6.0,\"4\":7.0,\"5\":9.0,\"6\":2382.0}}],\"msInterval\":100.0}";
        List<TrainingData.simple> testdataList = new ArrayList<>();
        //make a few datapoints
        Integer n;
        for (n = 0; n<10; n++) {
            TrainingData.simple trainingData = new TrainingData.simple();
            HashMap<Integer, Double> tdOutPuts = trainingData.outputs();
            tdOutPuts.put(1, 1d);
            tdOutPuts.put(2, 5d);
            tdOutPuts.put(3, 6d);
            tdOutPuts.put(4, 7d);
            tdOutPuts.put(5, 9d);
            tdOutPuts.put(6, 2382d);
            Double[] inputSet = new Double[]{
                    1d + n.doubleValue(),
                    2d + n.doubleValue(),
                    3d + n.doubleValue(),
                    4d + n.doubleValue(),
                    5d + n.doubleValue()};
            trainingData.setInputs(inputSet);
            testdataList.add(trainingData);
        }

        //import from string
        TrainingData.deltaT deltaTfromString = new TrainingData.deltaT(jsonString);
        //Check if the order and contents are the same as the original
        for (n = 0; n<testdataList.size(); n++) {
            assertTrue("#" + n + " was loaded correctly", deltaTfromString.get(n).equals(testdataList.get(n)));
        }
        assertTrue("interval was read correctly",deltaTfromString.getMsInterval().equals(100d));

    }

    @Test
    public void testDeltaTSaveLoad() throws Exception {
        List<TrainingData.simple> testdataList = new ArrayList<>();
        //make a few datapoints
        Integer n;
        for (n = 0; n<10; n++) {
            TrainingData.simple trainingData = new TrainingData.simple();
            HashMap<Integer, Double> tdOutPuts = trainingData.outputs();
            tdOutPuts.put(1, 1d);
            tdOutPuts.put(2, 5d);
            tdOutPuts.put(3, 6d);
            tdOutPuts.put(4, 7d);
            tdOutPuts.put(5, 9d);
            tdOutPuts.put(6, 2382d);
            Double[] inputSet = new Double[]{
                    1d + n.doubleValue(),
                    2d + n.doubleValue(),
                    3d + n.doubleValue(),
                    4d + n.doubleValue(),
                    5d + n.doubleValue()};
            trainingData.setInputs(inputSet);
            testdataList.add(trainingData);
        }

        TrainingData.deltaT deltaT = new TrainingData.deltaT();

        //add them to the deltaT
        for (TrainingData.simple trainingData: testdataList) {
            deltaT.add(trainingData);
        }

        //save it
        deltaT.save("./trainingsdataTest.json");

        TrainingData.deltaT loadedDeltaT = new TrainingData.deltaT();
        loadedDeltaT.load("./trainingsdataTest.json");

        //Check if the order and contents are the same as the original
        assertTrue("Saved and loaded are equal ",deltaT.equals(loadedDeltaT));

    }

    @Before
    public void setUp() throws Exception {
        setUpLogger();
    }

    private void setUpLogger() {
        Handler testHandler = new Handler() {
            @Override
            public void publish(LogRecord record) {
                System.out.print(getFormatter().format(record));
            }

            @Override
            public void flush() {
                System.out.flush();
                System.err.flush();
            }

            @Override
            public void close() throws SecurityException {
            }
        };

        Formatter logFormatter = new Formatter() {
            private String buildStackPath() {
                String stackPath = "";
                for (StackTraceElement elem:Thread.currentThread().getStackTrace()) {
                    if (elem.getClassName().startsWith(getClass().getPackage().getName())) {
                        stackPath = elem.getClassName() + "." + elem.getMethodName() + ":" + elem.getLineNumber() + "\n" + stackPath;
                    }
                }

                return stackPath;
            }
            @Override
            public String format(LogRecord record) {
                return record.getLevel() + "\t" + "[" + Thread.currentThread().getName() + "]" + record.getLoggerName()+"->"+record.getSourceMethodName() + " : \t" + record.getMessage()+ "\n";
//                        buildStackPath() + "\n";
            }
        };
        testHandler.setFormatter(logFormatter);
        logger.setUseParentHandlers(false);

        logger.addHandler(testHandler);
        logger.setLevel(Level.INFO);
    }

    @After
    public void tearDown() throws Exception {

    }

}