import com.langejansen.net.CellCollection;
import com.langejansen.net.support.GeneralSettings;
import com.langejansen.net.support.netConstructor;
import com.langejansen.trainer.Trainer;
import com.langejansen.trainer.TrainingData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by tim on 2-5-17.
 */
public class TrainerBackpropagatingTest {
    private Logger logger = null;

    public TrainerBackpropagatingTest() {
        setUpLogger();
    }

    private void setUpLogger() {
        logger = Logger.getLogger(getClass().getName());
        GeneralSettings.getInstance().generalInit();
    }

    private void assertFalse(String message, Boolean shouldbeFalse) {
        assertTrue(message, !shouldbeFalse);
    }

    private void assertTrue(String message, Boolean shouldbeTrue) {
        Logger logger = Logger.getLogger("com.langejansen");
        if (shouldbeTrue) {
            logger.info("Passed: " + message);
        } else {
            logger.severe("Failed: " + message);
            throw new Error("Failed: " + message);
        }
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void train() throws Exception {
        //create a net
        netConstructor netCreator = new netConstructor.uniform(100l, 1d, 1d, 10l);
        netCreator.setInputs(3);
        netCreator.setOutputs(1);
        CellCollection net = netCreator.create();
        //create test data
        TrainingData.deltaT td = new TrainingData.deltaT("{\"frames\":[" +
                "{\"inputs\":[0.0,0.0,0.0],\"outputs\":{\"0\":0.0}}" +
                ",{\"inputs\":[0.0,0.0,1.0],\"outputs\":{\"0\":1.0}}" +
                ",{\"inputs\":[0.0,1.0,0.0],\"outputs\":{\"0\":1.0}}" +
                ",{\"inputs\":[0.0,1.0,1.0],\"outputs\":{\"0\":0.0}}" +
                ",{\"inputs\":[1.0,0.0,0.0],\"outputs\":{\"0\":1.0}}" +
                ",{\"inputs\":[1.0,0.0,1.0],\"outputs\":{\"0\":0.0}}" +
                ",{\"inputs\":[1.0,1.0,0.0],\"outputs\":{\"0\":0.0}}" +
                ",{\"inputs\":[1.0,1.0,1.0],\"outputs\":{\"0\":1.0}}" +
                "],\"msInterval\":null}");
        //Make a new trainer
        Trainer.backpropagating trainer = new Trainer.backpropagating(net, td);
        //train
        GeneralSettings.getInstance().logger.setLevel(Level.FINEST);

        trainer.train();
        logger.setLevel(Level.INFO);
        //verify result
    }

}