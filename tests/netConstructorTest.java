import com.langejansen.net.CarrierCollection;
import com.langejansen.net.CellCollection;
import com.langejansen.net.cellClass;
import com.langejansen.net.support.AllCells;
import com.langejansen.net.support.GeneralSettings;
import com.langejansen.net.support.PulsePool;
import com.langejansen.net.support.netConstructor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

/**
 * Created by tim on 2-5-17.
 */
public class netConstructorTest {
    private Logger logger = null;
    private List<String> failedAsserts = new ArrayList<>();
    private List<String> passedAsserts = new ArrayList<>();

    public netConstructorTest() {
        setUpLogger();
    }

    private void setUpLogger() {
        logger = Logger.getLogger(getClass().getName());
        GeneralSettings.getInstance().generalInit();
    }

    @Before
    public void setUp() throws Exception {
        AllCells.getInstance().clear();
    }

    @After
    public void knockDown() throws Exception {
        if (passedAsserts.size() > 0) {
            String theMsg = "passed " + passedAsserts.size() + "assertions:";
            for (String singlepassed : passedAsserts) {
                theMsg += "\n - " + singlepassed;
            }
            theMsg += passedAsserts.size();
            theMsg += "\n\n";
            System.out.println(theMsg);
        }

        if (failedAsserts.size() > 0) {
            String errMsg = "Failed " + failedAsserts.size() + "assertions:";
            for (String singleFailed : failedAsserts) {
                errMsg += "\n - " + singleFailed;
            }
            errMsg += failedAsserts.size();
            throw new Exception(errMsg);
        }
    }

    private void assertFalse(String message, Boolean shouldbeFalse) {
        assertTrue(message, !shouldbeFalse);
    }

    private void assertTrue(String message, Boolean shouldbeTrue) {
        Logger logger = Logger.getLogger("com.langejansen");
        if (shouldbeTrue) {
            logger.info("Passed: " + message);
            passedAsserts.add("Passed: " + message);
        } else {
            logger.severe("Failed: " + message);
            failedAsserts.add("Failed: " + message);
        }
    }

    //    @Test
//    public void createUniform() throws Exception {
//        //todo: make a test for a generated layered uniform test
////        GeneralSettings.getInstance().logger.setLevel(Level.FINE);
//        for (Long size = 10l;size < 200l;size+=10) {
//            testuniform(size);
//        }
//    }
    @Test
    public void testfollowPulse() throws Exception {
//        GeneralSettings.getInstance().logger.setLevel(Level.FINER);
        testuniform(10000l);
    }

    private long testuniform(Long size) throws TimeoutException {
        logger.info("size = " + size + "\t");
        netConstructor.uniform uniform = new netConstructor.uniform(size, 1d, 1d, 10l);
        uniform.setOutputs(3);
        uniform.setInputs(5);
        uniform.nameInputs = true;
        CellCollection cc = uniform.create();
        HashMap<Integer, cellClass> ic = cc.getInputCells();
        assertTrue("size = " + size + "	5 inputs were created", ic.size() == 5);
        HashMap<Integer, cellClass> oc = cc.getOutputCells();
        assertTrue("size = " + size + "	3 outputs were returned/created", oc.size() == 3);

        List<cellClass> lyrs = AllCells.getInstance().getCells("[0-9]+.1");


        cc.applyInput(new Double[]{1d, 1d, 1d, 1d, 1d});
//        Long waitedFor = oc.get(0).timePulseMs(10);
//        ic.get(1).sendPulse(10d);
        CarrierCollection carriers = AllCells.getInstance().carriers();
        PulsePool queue = carriers.pulseQueue;
        Long ranFor = queue.runfor(120l);
        assertTrue("size = " + size + "	pulse arrived at end: potential = " + oc.get(2).getPotential() + " after " + ranFor + " seconds", oc.get(2).getPotential() > 0.0d);
//        AllCells.getInstance().carriers().pulseQueue.shutdown();
        cc.clear();
        return ranFor;
//        AllCells.getInstance().clear();
    }

    @Test
    public void createRandom() throws Exception {
        //todo: make a test for a generated random connected uniform test
    }

}